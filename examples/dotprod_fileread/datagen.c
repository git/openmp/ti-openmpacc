/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "ti_omp_device.h"

#define RAND_MAX_  256

void HandleOptions(int argc, char* argv[]);
void PrintUsageAndExit();

int datagen(const char *dataA_file_name,
            const char *dataB_file_name,
            const char *result_file_name,
            int size_MB)
{
    int i;
    int acc_device_id = 0;
    int size_bytes = size_MB*1024*1024;
    int num_elements = size_bytes/sizeof(float);  
    FILE* fp_A;
    FILE* fp_B;
    FILE* fp_result;
    /* ---------------------------------------------------------------- */
    /* Allocating memory in CMEM                                        */
    /* ---------------------------------------------------------------- */
    float* data_A     = (float*) __malloc_ddr( size_bytes);
    float* data_B     = (float*) __malloc_ddr( size_bytes); 
    float* result     = (float*) __malloc_ddr( size_bytes); 

    /* ---------------------------------------------------------------- */
    /* Generate data and calculate result                               */
    /* ---------------------------------------------------------------- */
    #pragma omp parallel default(none) \
			             private(i) \
			             firstprivate(num_elements) \
			             shared(data_A, data_B, result)
    {
        srand(time(NULL));
        #pragma omp for
        for (i=0; i < num_elements; ++i) 
        {
            data_A[i] = (rand() % RAND_MAX_ + 1) * 1.0; 
            data_B[i] = (rand() % RAND_MAX_ + 1) * 1.0;
            result[i] = data_A[i]*data_B[i];
        }
    }
    /* ---------------------------------------------------------------- */
    /* Open Files for writing                                           */
    /* ---------------------------------------------------------------- */
    fp_A = fopen(dataA_file_name, "w");
    fp_B = fopen(dataB_file_name, "w");
    fp_result = fopen(result_file_name, "w");
   
    /* ---------------------------------------------------------------- */
    /* Write the first byte -> size in MB                               */
    /* ---------------------------------------------------------------- */
    fprintf(fp_A,"%d",size_MB);
    fprintf(fp_B,"%d",size_MB);
    fprintf(fp_result,"%d",size_MB);

    /* ---------------------------------------------------------------- */
    /* Write the arrays                                                 */
    /* ---------------------------------------------------------------- */
    fwrite(data_A, sizeof(float), num_elements, fp_A);
    fwrite(data_B, sizeof(float), num_elements, fp_B);
    fwrite(result, sizeof(float), num_elements, fp_result);
    
    /* ---------------------------------------------------------------- */
    /* Close files                                                      */
    /* ---------------------------------------------------------------- */
    fclose(fp_A);
    fclose(fp_B);
    fclose(fp_result);

    /* ---------------------------------------------------------------- */
    /* Free allocated memory                                            */
    /* ---------------------------------------------------------------- */
    __free_ddr(data_A);
    __free_ddr(data_B);
    __free_ddr(result);

    return 0;
}

/**
 ** \brief Handle command line options
 ** \param argc  Number of options
 ** \param argv  Array containing option strings
 **/
