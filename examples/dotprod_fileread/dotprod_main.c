/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "ti_omp_device.h"

#define EPISILON                0.00001
#define NAME_LEN                20
#define BLOCK_SIZE_DEFAULT      2097152 // 2*1024*1024 floats = 8 MB
#define ARM_MAX_THREADS         4

char        srcA_file_name[NAME_LEN];
char        srcB_file_name[NAME_LEN];
char        result_file_name[NAME_LEN];
int         size_MB = 32;
int         block_size = BLOCK_SIZE_DEFAULT;
int 	    nthreads = ARM_MAX_THREADS; 

void        HandleOptions(int argc, char* argv[]);
void        PrintUsageAndExit();
extern void dotprod(float * srcA, float * srcB, float *dst, int size);

extern int datagen(const char *dataA_file_name,
            const char *dataB_file_name,
            const char *result_file_name,
            int size_MB);

int main(int argc, char* argv[])
{
    HandleOptions(argc, argv);
    FILE *srcA_fp[ARM_MAX_THREADS];
    FILE *srcB_fp[ARM_MAX_THREADS];
    FILE *result_fp;
    int i, num_errors = 0, acc_device_id = 0, ret = 0;
    int num_elems_per_thread, bufsize;
    int size_MB_A, size_MB_B, size_MB_result, num_elems;
    const int print_nerrors = 12; 
    long fp_offset_data_size_MB = 0;
    double start = 0, end = 0;

    datagen(srcA_file_name, srcB_file_name, result_file_name, size_MB);
    
    /* ---------------------------------------------------------------- */
    /* Read in array size data from files                               */
    /* ---------------------------------------------------------------- */
    omp_set_num_threads(nthreads);
    #pragma omp parallel default(none) \
                         shared(srcA_fp, srcB_fp) \
                         shared(srcA_file_name, srcB_file_name)
    {
        int tid = omp_get_thread_num();
        srcA_fp[tid] = fopen(srcA_file_name,"r");
        if (srcA_fp[tid] == NULL)
        {
            printf("[%d] Cannot open file %s\n",tid, srcA_file_name);
            exit(1);
        }
        srcB_fp[tid] = fopen(srcB_file_name,"r");
        if (srcB_fp[tid] == NULL)
        {
            printf("[%d] Cannot open file %s\n",tid, srcB_file_name);
            exit(1);
        }
    }
    result_fp = fopen(result_file_name,"r");
    if (result_fp == NULL)
    {
        fprintf(stderr, "Cannot open file %s\n", result_file_name);
        exit(1);
    }
    
    ret = fscanf(srcA_fp[0], "%d", &size_MB_A);
    ret &= fscanf(srcB_fp[0], "%d", &size_MB_B);
    ret &= fscanf(result_fp, "%d", &size_MB_result);
    
    if ( ret!=1 ||  (size_MB_A != size_MB_B) || (size_MB_B != size_MB_result))
	    PrintUsageAndExit();
    
    fp_offset_data_size_MB = ftell(srcA_fp[0]); 
    bufsize = size_MB_A*1024*1024;
    num_elems = bufsize/sizeof(float);
    /* ---------------------------------------------------------------- */
    /* Allocating memory in CMEM                                        */
    /* ---------------------------------------------------------------- */
    float* srcA     = (float*) __malloc_ddr(bufsize);
    float* srcB     = (float*) __malloc_ddr(bufsize); 
    float* dst      = (float*) __malloc_ddr(bufsize); 
    float* Golden   = (float*) __malloc_ddr(bufsize); 

    /* ---------------------------------------------------------------- */
    /* Zero out result matrix, and read in arrays into memory           */
    /* ---------------------------------------------------------------- */
    memset(dst, 0, bufsize);
    
    /* ---------------------------------------------------------------- */
    /* Call dotprod target code                                         */
    /* ---------------------------------------------------------------- */
    printf("Source file size (MB)          : %d\n", size_MB_A);
    printf("Block size (MB)                : %d\n", sizeof(float)*block_size/(1024*1024));
    printf("Number of Elements             : %d\n", num_elems);
    printf("Number of Elements per block   : %d\n", block_size);
    for (nthreads = 1; nthreads < 5; nthreads++)
    {
        if (nthreads == 3) continue;
        omp_set_num_threads(nthreads);
        num_elems_per_thread = num_elems/nthreads;
        printf("-------------------------------------------\n");
        printf("Number of ARM threads          : %d\n", nthreads);
        printf("Number of blocks per thread    : %d\n", num_elems_per_thread/block_size);
        start = omp_get_wtime();
        #pragma omp parallel default(none) \
                             shared(srcA_fp, srcB_fp) \
                             firstprivate(nthreads, num_elems, num_elems_per_thread, block_size, fp_offset_data_size_MB) \
                             shared(srcA, srcB, dst)
        {
            int i;
            int tid = omp_get_thread_num();
            int tindex = tid*num_elems_per_thread;
            int num_blocks_per_thread = num_elems_per_thread/block_size;
            int data_read_A=0, data_read_B=0;
            double read_start = 0, read_end = 0, proc_start = 0, proc_end = 0;
	        double read_total = 0, proc_total = 0;
            for (i = 0; i<num_blocks_per_thread; i++)
            {
                fseek(srcA_fp[tid], fp_offset_data_size_MB + tindex*sizeof(float), SEEK_SET);
                fseek(srcB_fp[tid], fp_offset_data_size_MB + tindex*sizeof(float), SEEK_SET);
                read_start = omp_get_wtime(); 
                data_read_A = fread(&srcA[tindex], sizeof(float), block_size, srcA_fp[tid]); 
                data_read_B = fread(&srcB[tindex], sizeof(float), block_size, srcB_fp[tid]);
                read_end = omp_get_wtime();
                read_total += read_end - read_start;
                #pragma omp critical
                {
                    /*
                       printf("[%d] Dispatching block %d - data_A_size: %dMB, data_B_size: %dMB\n",
                              tid, i, 
                              (data_read_A*sizeof(float))/(1024*1024),
                              (data_read_B*sizeof(float))/(1024*1024)
                              );
                    */
                    proc_start = omp_get_wtime();
                    dotprod(srcA + tindex,
                            srcB + tindex,
                            dst + tindex,
                            block_size);
                    proc_end = omp_get_wtime();
                    proc_total += proc_end - proc_start;
                    /*
                       printf("[%d] Block %d (2*%dMB): Reading took %fs, Processing took %fs\n",
                              tid, i, (data_read_A*sizeof(float))/(1024*1024), 
                              read_end - read_start, proc_end - proc_start
                              );
                    */
                }
                tindex+=block_size;
            }
            printf("[%d] Total read time            : %f\n"
                    "    Total processing time      : %f\n", 
                    tid, read_total, proc_total
                  );
        }
        end = omp_get_wtime();
        printf("Total Elapsed time             : %f\n", end - start);
    }
    /* ---------------------------------------------------------------- */
    /* Perform error checking                                           */
    /* ---------------------------------------------------------------- */
    // Read in result array from file
    printf("Reading in result array..\n"); 
    start = omp_get_wtime();
    ret = fread(Golden, sizeof(float), num_elems, result_fp); 
    end = omp_get_wtime();
    printf("Reading %dMB took %fs\nStarting check..\n", 
	   (ret*sizeof(float))/(1024*1024),
	   end - start
           );
    for (i=0; i < num_elems; ++i)
    {
        if (Golden[i] - dst[i] < -EPISILON || Golden[i] - dst[i] > EPISILON) 
        { 
            if((num_errors += 1) < print_nerrors)
                printf("Error %d: %f <==> %f\n", i, Golden[i], dst[i]);
        }
    }
    if (num_errors > 0)  printf("FAIL with %d errors!\n",num_errors);
    else                 printf("PASS!\n"); 
    
    /* ---------------------------------------------------------------- */
    /* Close file pointers                                              */
    /* ---------------------------------------------------------------- */
    fclose(srcA_fp[0]);
    fclose(srcB_fp[0]);
    fclose(result_fp);

    /* ---------------------------------------------------------------- */
    /* Free allocated memory                                            */
    /* ---------------------------------------------------------------- */
    __free_ddr(srcA);
    __free_ddr(srcB);
    __free_ddr(dst);
    __free_ddr(Golden);

    return 0;
}

/**
 ** \brief Handle command line options
 ** \param argc  Number of options
 ** \param argv  Array containing option strings
 **/
void HandleOptions(int argc, char* argv[])
{   
    if (argc > 6)
        PrintUsageAndExit();
    
    if (argc > 3)
    {
        strcpy(srcA_file_name, argv[1]);
        strcpy(srcB_file_name, argv[2]);
        strcpy(result_file_name, argv[3]);
    }
    else
    {
        strcpy(srcA_file_name, "dataA");
        strcpy(srcB_file_name, "dataB");
        strcpy(result_file_name, "dataRes");
    }

    if (argc > 4)
    {
        size_MB = abs(atoi(argv[4]));        
    }

    if (argc > 5)
    {
        block_size = abs(atoi(argv[5]))*1024*1024/sizeof(float);        
    }
}

/**
 *  * \brief Print Help Message and Exit
 *   */
void PrintUsageAndExit()
{   
    printf("Dot-product fileread\n");
    printf("Usage      : ./dotprod data_A data_B result [block size]\n");
    printf("block size : Block size (in MB) to be read at a time by a thread\n");
    printf("Input files must have been generated using datagen\n");
    printf("Four threads on ARM will read blocks from files\n");
    printf("Default block size is set to 8 MB, for a 32 MB test case\n");
    printf("Partitioning: x = data_size/4 MB per thread, \n");
    printf("              y = x/block_size blocks processed per thread\n");
    printf("              Ensure that (x mod block_size) = 0 \n");
    exit(0);
}
