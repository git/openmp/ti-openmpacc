#*****************************************************************************
# Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
#   All rights reserved.
#
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#       * Redistributions of source code must retain the above copyright
#         notice, this list of conditions and the following disclaimer.
#       * Redistributions in binary form must reproduce the above copyright
#         notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution.
#       * Neither the name of Texas Instruments Incorporated nor the
#         names of its contributors may be used to endorse or promote products
#         derived from this software without specific prior written permission.
#
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  
#   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
#   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
#   THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************

# Clean the examples directory so that the install package is clean.
# Assuming that openmpacc-examples directory is present otherwise will get a
# cmake error. 
# NOTE: SHOULD REMOVE ANY INSTALL PACKAGE PRIOR TO DOING A PRODUCTION INSTALL
#install(CODE "execute_process(COMMAND make clean WORKING_DIRECTORY ${OMPACC_EXAMPLES_DIR})")
SET (OMPACC_EXAMPLES_PATH           /usr/share/ti/examples/openmpacc)

# Install the top-level directory, Makefiles, setup script
INSTALL(DIRECTORY DESTINATION ${OMPACC_EXAMPLES_PATH} ${OMPACC_DPERMS})
INSTALL(FILES make.inc Makefile DESTINATION ${OMPACC_EXAMPLES_PATH} ${OMPACC_FPERMS})

# Get all subdirectories (examples) in the examples repo
FILE(GLOB OMPACC_EXAMPLES_FILE_LIST ${OMPACC_EXAMPLES_DIR}/*)
FOREACH(filename ${OMPACC_EXAMPLES_FILE_LIST})
    IF(IS_DIRECTORY ${filename} AND NOT ${filename} MATCHES "\\.git")
        SET(OMPACC_EXAMPLES_LIST ${OMPACC_EXAMPLES_LIST} ${filename})
    ENDIF()
ENDFOREACH()

# Loop through the subdirectory list and install
FOREACH(subdir ${OMPACC_EXAMPLES_LIST})
    INSTALL(DIRECTORY ${subdir} DESTINATION ${OMPACC_EXAMPLES_PATH} USE_SOURCE_PERMISSIONS)
ENDFOREACH()
