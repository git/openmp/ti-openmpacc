Change Log
**********

1.5.6.0, Processor SDK 6.2
  * Documentation clarification on unsupported variadic functions.

1.3.0, Released 04-12-2016
  * Fixed memory leak with mapping variables
  * Added support for AM572, 66AK2L, 66AK2E and 66AK2G platfroms

1.1.0, Released 09-10-2014
  * Integrated TI's Usage and Load Monitor library (ULMLib). ULMLib collects specific OpenMP runtime events into DSP trace buffers. More details here
  * Non-OpenMP pragmas within target regions are now supported
  * *default* clause on constructs is now supported
  * Long long type for loop iteration variables is now supported for OpenMP loops
  * An empty statement is no longer required to follow a target update construct

1.1.0, Released 07-30-2014 
  * Non-zero lower bounds for array sections is now supported
  * Mapping sub-array sections of mapped array sections is now supported
  * Added examples dotprod_fileread, edmabw, local, sub_section, vecadd_complex, vecadd_lib
  * Device and if clauses are now supported on device constructs
  * Memory management functions __TI_omp_device_alloc and __TI_omp_device_free have been replaced by __malloc_ddr()/__malloc_msmc() and __free_ddr()/__free_msmc()
  * Creation of clacc static libraries is now supported
  * Included man page for CLACC

0.3.3, Released  04-25-2014  
  * Fixed target region without map clause problem in parser, removed error description in readme
  * Added examples target_implicit_map, target_orphan_call, target_update, dotprod, openmpbench_C_v3
  * Performance optimization: CMEM cacheWb and CMEM_cacheInv's done only when needed
  * Supports applications written entirely in C
