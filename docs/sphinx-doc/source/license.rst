License
*******

This work is licensed under the Creative Commons Attribution-NoDerivs
3.0 Unported License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-nd/3.0 or send a letter to
Creative Commons, 171 Second Street, Suite 300, San Francisco,
California 94105, USA.
