.. _ompa_examples:

Examples
********

The OpenMPAcc examples are installed in the directory ``/usr/share/ti/examples/openmpacc``.

**printf_debug**

 Demonstrates the use of printf() within an OpenMP target region for debugging. This example also demonstrates the use of OpenMP runtime library functions.

**dotprod**

 Performs vector dot product using C66x intrinsic functions within target region. This example is written entirely in C. It also depends on the c_intrinsics_host_port package. Instructions are included in the Makefile.

**dsplib_fft**

 Demonstrates the use of C66x libraries with the OpenMP Accelerator Model. It has a target region which calls a DSPLIB library function.

**edmamgr**

 Demonstrates the use of EDMA Manager library functions to do accelerated memory transfers within an OpenMP target region. EDMA Manager is used to perform 1D and 2D memory transfers using EDMA3 co-processors.

**vecadd**

 Small vector addition using a single OpenMP parallel region offloaded to the DSP using #pragma omp target.

**vecadd_t**

 Small vector addition using OpenMP tasks within an OpenMP parallel region offloaded to the DSP using #pragma omp target.

**vecadd_complex**

  Similar to vecadd, but elements of the vectors are of complex type.

**local**

 Demonstrates the use of the local map type. This example is similar to vecadd, but uses an intermediate local buffer (in L2 SRAM) to accumulate the sum of the input vectors.

**sub_section**

 Demonstrates the use of array sections. This example is similar to vecadd, but the vector addition is achieved in two steps with each step accumulating the sum of subsections of the vectors.

**null**

 Measures time overhead associated with offloading a single target region from ARM to DSP.

**target_implicit_map**

 Demonstrates implicit mapping of array sections when offloading target regions within a '#pragma omp target data' region.

**target_orphan_call**

 Demonstrates placing a target region within a function and calling this function from within a '#pragma omp target data' region.

**target_update**

 Demonstrates how to use the '#pragma omp target update' clause to synchronize host and device array sections within a '#pragma omp target data' region.

**dotprod_fileread**

 Demonstrates how target regions may be used within a parallel region. Four OpenMP threads on the ARM concurrently read the input arrays. Then each thread offloads the computation of the dot product of its input to the DSPs, one at a time, within a critical region.
