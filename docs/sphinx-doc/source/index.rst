##################################
TI OpenMP Accelerator Model v1.3.x
##################################

TI |OpenMP (R)| Accelerator Model Documentation Contents:

.. toctree::
   :maxdepth: 2
   :numbered:

   intro
   getting_started
   supported_constructs
   ti_extensions
   dynamic_memory_management
   examples
   change_log
   disclaimer

.. image:: images/platform_red.png

.. |OpenMP (R)| unicode:: OpenMP U+00AE
