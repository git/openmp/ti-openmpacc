#!/bin/bash

# Check if required boost options are set
: ${BOOST_VERSION:?"BOOST_VERSION not set"}
: ${BOOST_BUILD_LIBS:?"BOOST_BUILD_LIBS not set"}
: ${BOOST_ROOT_ARM:?"BOOST_ROOT_ARM not set"}
: ${BOOST_ROOT_X86:?"BOOST_ROOT_X86 not set"}

BOOST_DL_LINK_VERSION=`echo ${BOOST_VERSION} | sed -e 's|_|.|g'` 
BOOST_SRC=boost_${BOOST_VERSION}
BOOST_TARBALL=${BOOST_SRC}.tar.gz
BOOST_DL_LOCAL=/cgnas/${BOOST_TARBALL}
BOOST_DL_ROOT=http://sourceforge.net/projects/boost/files/boost
BOOST_DL_URL=${BOOST_DL_ROOT}/${BOOST_DL_LINK_VERSION}/${BOOST_TARBALL}/download
BOOST_ARM_BUILD_LOG=boost_arm.log
BOOST_X86_BUILD_LOG=boost_x86.log

CC_X86=g++
CC_ARM=arm-linux-gnueabihf-g++

# Check if boost package already downloaded
if [ -e ${BOOST_TARBALL} ]
then
    echo "Boost tarball: ${BOOST_TARBALL} already downloaded"
else
    DL_BOOST_TARBALL=1
fi

# Check if boost src is already extracted
if [ -d "${BOOST_SRC}" ]
then
    echo "Boost source already extracted"
else
    EXTRACT_BOOST_SRC=1
fi

# Check if boost_x86 already installed
if [ -d "${BOOST_ROOT_X86}" ]
then
    # Check for each static lib that is required
    IFS=',' read -ra LIBS <<< "${BOOST_BUILD_LIBS}"
    for lib in "${LIBS[@]}"
    do
        if [ ! -e ${BOOST_ROOT_X86}/lib/libboost_${lib}.a ]
        then
            BUILD_BOOST_X86=1
            break
        fi
    done
else
    BUILD_BOOST_X86=1
fi

# Check if boost_arm already installed
if [ -d "${BOOST_ROOT_ARM}" ]
then
    # Check for each static lib that is required
    IFS=',' read -ra LIBS <<< "${BOOST_BUILD_LIBS}"
    for lib in "${LIBS[@]}"
    do
        if [ ! -e ${BOOST_ROOT_ARM}/lib/libboost_${lib}.a ]
        then
            BUILD_BOOST_ARM=1
            break
        fi
    done
else
    BUILD_BOOST_ARM=1
fi

# Download boost source tarball if not already downloaded
if [ ${DL_BOOST_TARBALL} ]
then 
    echo "************************************"
    echo "**** Downloading Boost *************"
    echo "************************************"
    if [ -e ${BOOST_DL_LOCAL} ]
    then
        cp ${BOOST_DL_LOCAL} .
    else
        wget "${BOOST_DL_URL}" -O ${BOOST_TARBALL}
    fi
    echo "************************************"
    echo "**** Downloaded Boost **************"
    echo "************************************"
fi

# Extract boost source if building required for x86 or ARM 
if [ ${BUILD_BOOST_X86} ] || [ ${BUILD_BOOST_ARM} ]
then
    if [ ${EXTRACT_BOOST_SRC} ]
    then
        echo "************************************"
        echo "**** Extracting Boost **************"
        echo "************************************"
        tar xzf ${BOOST_TARBALL}
        echo "************************************"
        echo "**** Extracted Boost ***************"
        echo "************************************"
        # Prep boost toolset options
        cd ${BOOST_SRC}
        echo "using gcc : x86 : ${CC_X86} ;" >> tools/build/src/user-config.jam
        echo "using gcc : arm : ${CC_ARM} ;" >> tools/build/src/user-config.jam
        ./bootstrap.sh --with-libraries=${BOOST_BUILD_LIBS}
    else
        cd ${BOOST_SRC} 
    fi
else
    echo "Boost already built and installed"
    exit 0
fi

# Build and install boost_x86 if required
if [ ${BUILD_BOOST_X86} ]
then
    echo "************************************"
    echo "**** Building boost_x86 ************"
    echo "************************************"
    if [[ ${BOOST_ROOT_X86} == *"_64"* ]] 
    then
        ./b2 install --prefix=../${BOOST_ROOT_X86} toolset=gcc-x86 address-model=64 > ${BOOST_X86_BUILD_LOG} 2>&1
    else
        ./b2 install --prefix=../${BOOST_ROOT_X86} toolset=gcc-x86 address-model=32 > ${BOOST_X86_BUILD_LOG} 2>&1
    fi
    echo "************************************"
    echo "**** Built boost_x86 ***************"
    echo "************************************"
else
    echo "boost_x86 already installed"
fi

# Build and install boost_arm if required
if [ ${BUILD_BOOST_ARM} ]
then
    echo "************************************"
    echo "**** Building boost_arm ************"
    echo "************************************"
    ./b2 install --prefix=../${BOOST_ROOT_ARM} toolset=gcc-arm > ${BOOST_ARM_BUILD_LOG} 2>&1
    echo "************************************"
    echo "**** Built boost_arm ***************"
    echo "************************************"
else
    echo "boost_arm already installed"
fi
