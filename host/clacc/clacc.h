/******************************************************************************
 * Copyright (c) 2019, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 * \file clacc.h
 * \brief Compiler for OpenMP 4.0 Accelerator Model programs
 */

#ifndef CLACC_H_
#define CLACC_H_

#ifndef CLACC_USE_ONLY
#error This header file is for use in CLACC implementation only
#endif

#include "compiler.h"
#include "elfutils.h"
#include <stdio.h>
#include <algorithm>

/*********************************************************************/
/* Pre-processor Macros                                              */
/*********************************************************************/
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

/*********************************************************************/
/* Global variables                                                  */
/*********************************************************************/
bool debug                          = false;
bool runtime_verbose                = false;
bool runtime_performance            = false;
bool keep_tmp_files                 = false;
bool full_host_execution            = false;
bool making_library                 = false;
#ifdef BUILD_DATE
std::string build_date              = std::string(TOSTRING(BUILD_DATE));
#endif
#ifdef PACKAGE_VERSION
std::string clacc_version           = std::string(TOSTRING(PACKAGE_VERSION));
#endif
#ifdef CLACC_CC
std::string host_cc                 = std::string(TOSTRING(CLACC_CC));
#else
#error CLACC_CC undefined. Cannot configure clacc.h.
#endif
#ifdef CLACC_CXX
std::string host_link_cc            = std::string(TOSTRING(CLACC_CXX));
#else
#error CLACC_CXX undefined. Cannot configure clacc.h.
#endif
std::string target_cc               = "cl6x";
std::string cl_link_cc              = "clocl";
std::string exe_name                = "fat_binary";
std::string gcc_predef_file_name;
std::string chained_cl_file_name    = "__TI_CLACC_KERNEL.cl";
std::string ti_ocl_cgt_env_var      = "TI_OCL_CGT_INSTALL";
std::string ti_rootdir_env_var      = "TARGET_ROOTDIR";
std::string runtime_dbg_var         = "__TI_show_debug_";
std::string runtime_perf_var        = "__TI_show_perf_";
std::string ti_cgt_path             = "/usr/share/ti/cgt-c6x";
std::string oa_inc_path             = "/usr/share/ti/openmpacc/dsp/include";
std::string ti_rootdir_path         = "";

std::string hc;

std::vector<std::string> hl;
std::string hl_variable;

std::vector<std::string> tc;
std::string tc_variable;

std::string tl;

std::vector<std::string> clocl_opts;
std::vector<std::string> s2s_opts;
std::vector<std::string> s2s_opts_variable;
std::vector<std::string> host_cc_inc_paths;

std::vector<std::string> host_objs;
std::vector<std::string> user_host_objs;
std::vector<std::string> target_objs;
std::vector<std::string> input_files;

std::vector<fs::path> extracted_clacc_lib_dirs;

po::variables_map vm;

/*********************************************************************/
/* Function Prototypes                                               */
/*********************************************************************/
/**
 * \brief Print usage or help message
 * \param visible Boost program options description instance
 */
void PrintUsage(po::options_description& visible);

/**
 * \brief Check and get required environment variables
 *
 * Check and get values of TI_OCL_CGT_INSTALL, TARGET_ROOTDIR
 * if they are set
 */
void GetEnvVars();

/**
 * \brief Initialize Compiler Options for all compilers being used
 */
void InitCompilerOpts();

/**
 * \brief Setup Command Line parser and print help if required
 * \param argc  Number of arguments to parse
 * \param argv  Pointers to argument buffers to parse
 * \return Boost program options description instance
 */
po::options_description SetupCommandLineParser(int argc, char* argv[]);

/**
 * \brief Handle command line options
 */
void HandleCommandLineOpts();

/**
 * \brief Check input files and sort them appropriately
 * \param input_c_files Vector reference to add C source files to
 *
 * Check each given input file for existence and separate them
 * into lots of C source files and ARM host and C66 target
 * object files
 */
void CheckAndSeparateInputFiles(std::vector<std::string>& input_c_files);

/**
 * \brief Create object files for target
 * \param t_cc          Target device compiler instance reference
 * \param input_c_files Reference to vector of input C source files
 * \param tmp_list      Reference to vector of temporary files being
 *                      generated during CLACC processes
 */
void MakeTargetObjs(Compiler& t_cc,
                    std::vector<std::string>& input_c_files,
                    std::vector<std::string>& tmp_list
                    );

/**
 * \brief Perform S2S Lowering to create host int.c and .cl files
 * \param s2s           S2S lowering compiler instance reference
 * \param input_c_files Reference to vector of input C source files
 * \param cl_chain      Reference to vector of cl file names being
 *                      generated during S2S lowering
 * \param h_int_list    Reference to vector of host int.c files being
 *                      generated during S2S lowering
 * \param tmp_list      Reference to vector of temporary files being
 *                      generated during CLACC processes
 * \param s2s_stage_pass Record of return values from s2s compilation
 */
void DoS2SLowering(Compiler& s2s,
                   std::vector<std::string>& input_c_files,
                   std::vector<std::string>& cl_chain,
                   std::vector<std::string>& h_int_list,
                   std::vector<std::string>& tmp_list
                   );

/**
 * \brief Create OpenCL program binary
 * \param clocl         OpenCL compiler and linker instance reference
 * \param h_link_cc     Host compiler and linker instance reference
 * \param cl_chain      Reference to vector of cl file names being
 *                      generated during S2S lowering
 * \param tmp_list      Reference to vector of temporary files being
 *                      generated during CLACC processes
 *
 * Chain all generated .cl files together (if present) and
 * run CLOCL on the chained cl file with the target object files to
 * create a header file with a char array containing the cl program
 * binary. Encode the lenght of the char array and the array in a C
 * file and compile the c file to an object file for final stage link
 */
void MakeCloclArtifacts(Compiler& clocl,
                        Compiler& h_link_cc,
                        std::vector<std::string>& cl_chain,
                        std::vector<std::string>& tmp_list
                        );

/*
 * \brief Create library with a single .cl, all .obj's and all .o's
 * \param h_cc          Host C compiler instance reference
 * \param h_int_files   Reference to vector of host int.c files being
 *                      generated during S2S lowering
 * \param tmp_list      Reference to vector of temporary files being
 *                      generated during CLACC processes
 * */
void MakeClaccLibrary(Compiler& h_cc,
                      std::vector<std::string>& h_int_files,
                      std::vector<std::string>& tmp_list
                      );

/**
 * \brief Perform final linking into fat binary
 * \param h_cc          Host C compiler instance reference
 * \param h_link_cc     Host compiler and linker instance reference
 * \param input_c_files Reference to vector of input C source files
 * \param h_int_files   Reference to vector of host int.c files being
 *                      generated during S2S lowering
 * \param tmp_list      Reference to vector of temporary files being
 *                      generated during CLACC processes
 *
 * \note If no cl files were generated then full host execution is
 *       enabled and an ARM only executable is created
 */
void DoFinalLink(Compiler& h_cc,
                 Compiler& h_link_cc,
                 std::vector<std::string>& h_int_files,
                 std::vector<std::string>& tmp_list
                 );

/**
 * \brief Clean up temporary files and print some debug info
 * \param tmp_list      Reference to vector of temporary files being
 *                      generated during CLACC processes
 *
 * Unless -k is set, remove all intermediate files and if debug is
 * set then report if the fat binary was created or not
 */
void CleanUp(std::vector<std::string>& tmp_list);

/**
 * \brief Get include paths from host compiler to be used with s2s
 */
void GetHostIncludePaths();

/**
 * \brief Get predefined macros from host compiler to be used with s2s
 * \param tmp_list      Reference to vector of temporary files being
 *                      generated during CLACC processes
 */
static void GetHostPreDefMacros(std::vector<std::string>& tmp_list);

#endif /* CLACC_H_ */
