/******************************************************************************
 * Copyright (c) 2019, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 * \file elfutils.h
 * \brief Wrapper for libelf utilities to examine ELF object files
 */

#ifndef ELFUTILS_H_
#define ELFUTILS_H_

#include <gelf.h>
#include <linux/elf-em.h>
#include <libelf.h>
#include <fcntl.h>
#include <unistd.h>
#include "util.h"

const std::string archiver = "ar" ;

/* ELF MACHINE ID FOR TI C6X DSPs */
/* TODO: Once build platforms are upgraded to Ubuntu 13.04+,
 * remove this define */
#ifndef EM_TI_C6000
#define EM_TI_C6000 140
#endif

/**
 * \brief Initialize libelf
 */
void ElfInit();

/**
 * \brief Open a file for reading as an elf file
 * \param fd        Pointer to file descriptor storage
 * \param file_name Name of file to open
 * \return Elf pointer to file
 */
Elf* OpenElfFile(int *fd, const std::string& file_name);

/**
 * \brief Close an Elf file
 * \param fd        Pointer to file descriptor storage
 * \param elf_file  Elf pointer to file
 */
void CloseElfFile(int *fd, Elf* elf_file);

/**
 * \brief Check if given file is an elf object file or archive
 * \param file Name of file to check
 * \return true if it is an ELF object file or archive, false if not
 */
bool IsElf(const std::string& file);

/**
 * \brief Check if given file is an elf object file
 * \param file Name of file to check
 * \return true if it is an ELF object file, false if not
 */
bool IsElfObj(const std::string& file);

/**
 * \brief Check if given file is an ELF library created by CLACC
 * \param file Name of file to check
 * \return true if it is a CLACC lib, false if not
 */
bool IsClaccLib(const std::string& file);

/**
 * \brief Get the machine ID of the given elf object file or archive
 * \param file Name of an elf object file or archive to get
 *             machine id for
 * \return ELF machine ID of the given elf file
 */
int GetElfMachineID(const std::string& file);

/**
 * \brief Extract the given Clacc library archive in a temporary folder
 *        and return the filesystem path to the folder
 * \param file  Name of the Clacc library archive to extract
 * \return Filesystem path to the folder with contents of the library
 */
fs::path ExtractClaccLib(const std::string& file);

/**
 * \brief Get a list of object files of given elf machine id in
 *        the given directory
 * \param dir        Filesystem directory to look in
 * \param elf_id     Elf machine ID to look for
 * \return Vector of filesystem paths of matching files
 */
std::vector<std::string> GetListOfElfObjs(fs::path dir, int elf_id);

#endif /* ELFUTILS_H_ */
