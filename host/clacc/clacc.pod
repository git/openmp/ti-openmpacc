=head1 NAME

clacc - TI OpenMP Accelerator Model Compiler

=head1 SYNOPSIS

clacc [options] [object-files] C-source-files

=head1 DESCRIPTION

CLACC is a wrapper shell that can be used to compile OpenMP 4.0 accelerator model source code for
the TI Keystone II "Hawking" ARM-DSP System-on-chip. CLACC is dependent on g++ for ARM
(arm-linux-gnueabihf-g++), TI Code Generation Tools (CGT) and the TI OpenCL library.

For more information regarding the OpenMP accelerator model see:
L<OpenMP 4.0 Specification|http://www.openmp.org/mp-documents/OpenMP4.0.0.pdf>. For more information regarding the TI's
implementation of the accelerator model and supported features see:
L<TI OpenMP Accelerator Model|http://processors.wiki.ti.com/index.php/OpenMP_Accelerator_Model>.

=head1 OPTIONS

=over 8

=item I<-h [ --help ]>

Show help message

=item I<-v [ --verbose ]>

Show debug output during clacc compilation

=item I<-k [ --keep_files ]>

Keep intermediate temporary files during clacc compilation

=item I<-d [ --runtime_verbose ]>

Enable runtime debugging information display. Setting this enables detailed runtime debug
information printing from the accelerator model ARM runtime library.

=item I<-p [ --runtime_performance ]>

Enable runtime performance information display. Setting this displays time taken to execute OpenMP
target regions and time taken to transfer data to and from the accelerator device at runtime.

=item I<-g [ --debug ]>

Generate target debug symbols that may be used with gdb-c6x for runtime debugging.

=item I<--make_lib>

This is used to create a CLACC static library which packages the ARM and DSP object files and
required dependencies. This library may only be used with CLACC and cannot be passed to the host or
accelerator device side compiler directly.

=item I<--host_cc> <arg>

Host compiler to use. Default is arm-linux-gnueabihf-g++.

=item I<--hc> <arg>

Host compiler options to be passed to the final compile and link stage.
E.g. --hc="-O3 -Wall -Wextra -fopenmp"

=item I<--tc> arg

Target compiler options to be passed to the accelerator device compile stage.
E.g. --tc="-O3 -k -symdebug:none"

=item I<--hl> arg

Host linker options to be passed to the final link stage.
E.g. --hl="-lrt -L/path/to/atlas/library/install/for/ARM -lptcblas -latlas"

=item I<--tl> arg

Target linker options to be passed to the accelerator model device link stage.
E.g. --tl="-L/path/to/static/library/for/C66x/DSP -ldsplib.ae66"

=item I<--show_cc_opts>

Show host and target compiler options being used.

=item I<-o [ --exe ]> <arg>

Name of final executable fat binary to be created. In case of library mode, this is the name of the
CLACC library to be generated.

=back

=head1 RETURN VALUE

Returns 0 for success and EXIT_FAILURE otherwise.

=head1 EXAMPLES

Typical Makefile usage of CLACC:

 OA_SHELL               = clacc
 CPP                    = arm-linux-gnueabihf-g++
 OA_SHELL_TMP_FILES     = *.out __TI_CLACC_KERNEL.c *.cl *.asm *.dsp_h *.bc *.objc *.if *.map *.opt *.int.c *.o *.obj
 OA_TC_OPTS             = -O3
 OA_HC_OPTS             = -O3 -Wall -Wextra -fopenmp
 OA_SHELL_OPTS          = -v -k --hc="$(OA_HC_OPTS)" --tc="$(OA_TC_OPTS)"
 CPP_OPTS               = -O3

 EXE                    = openmpacc_program
 HOST_CODE              = openmpacc_program_main.cpp
 TARGET_CODE            = openmpacc_program_target.c
 OBJS                   = $(patsubst %.cpp,%.o,$(HOST_CODE))

 $(EXE): $(OBJS)
	$(OA_SHELL) $(OA_SHELL_OPTS) $(OBJS) $(TARGET_CODE) -o $@

 %.o: %.cpp
	$(CPP) $(CPP_OPTS) -c $<

 clean:
	@rm -f $(EXE) $(OA_SHELL_TMP_FILES)

=head1 ENVIRONMENT

CLACC accepts two environment variables:

=over 8

=item B<TI_OCL_CGT_INSTALL>

Set this environment variable to point to TI Code Generation Tools (CGT) to be used to compile code
for the TI C66x DSP. If this variable is not set, CLACC will look for the CGT includes and
libraries in /usr/share/ti/cgt-c6x.

=item B<TARGET_ROOTDIR>

Set this environment variable to point to the TI MCSDK root filesystem location if cross compiling
on an x86 host machine. If compiling natively on ARM, then do not set this variable.

=back

=head1 NOTES

CLACC libraries created with --make_lib should not be passed in through arguments --hl or --tl.
They should simply be passed in to the CLACC command-line. For e.g.:

    clacc --hc="-O3 -Wall -Wextra -fopenmp" --hl="-lrt" --tc="-O3" main.o clacc_lib.a -o exe

Options passed though --hc, --hl, --tc, --tl are passed directly to their respective compilers i.e.
--hc and --hl options are passed to the host compiler (arm-linux-gnueabihf-g++) and --tc and --tl
are passed to the accelerator device compiler (cl6x).

Standard ELF object files and static libraries (ARM, C66x) may also be passed to the CLACC
command-line. The order in which they are passed is preserved and they are passed to the host or
target compiler before the --hl or --tl options are passed. For e.g.:

    clacc --hc="-O3 -Wall -Wextra -fopenmp" --hl="-lrt" --tc="-O3" main.o host_static_lib.a target.c -o exe

would pass the options to arm-linux-gnueabihf-g++ in the following manner:

    arm-linux-gnueabihf-g++ -O3 -Wall -Wextra -fopenmp main.o host_static_lib.a __TI_CLACC_KERNEL.o target.int.o -lrt -L/usr/lib -lOpenMPAcc -lOpenCL -locl_util -Wl,-rpath-link,/usr/lib -o exe

=head1 KNOWN_ISSUES

Please see
L<Known Issues & Limitations|http://processors.wiki.ti.com/index.php/OpenMP_Accelerator_Model#Known_Issues_.26_Limitations>
for a list of current restrictions and issues.

=head1 AUTHOR

Texas Instruments Inc.

=head1 SEE_ALSO

I<arm-linux-gnueabihf-g++>(1), arm-linux-gnueabihf-gcc(1)

=head1 COPYRIGHT

    /******************************************************************************
    * Copyright (c) 2019, Texas Instruments Incorporated - http://www.ti.com/
    *   All rights reserved.
    *
    *   Redistribution and use in source and binary forms, with or without
    *   modification, are permitted provided that the following conditions are met:
    *       * Redistributions of source code must retain the above copyright
    *         notice, this list of conditions and the following disclaimer.
    *       * Redistributions in binary form must reproduce the above copyright
    *         notice, this list of conditions and the following disclaimer in the
    *         documentation and/or other materials provided with the distribution.
    *       * Neither the name of Texas Instruments Incorporated nor the
    *         names of its contributors may be used to endorse or promote products
    *         derived from this software without specific prior written permission.
    *
    *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
    *   THE POSSIBILITY OF SUCH DAMAGE.
    *****************************************************************************/
