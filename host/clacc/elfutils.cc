/******************************************************************************
 * Copyright (c) 2019, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 * \file elfutils.cpp
 * \brief Wrapper for libelf utilities to examine ELF object files
 */

#include "elfutils.h"

/*********************************************************************/
/* Initialize libelf                                                 */
/*********************************************************************/
void ElfInit()
{
    if (elf_version(EV_CURRENT) == EV_NONE)
        std::cerr << ">> ERROR: libelf init failed" << std::endl;
}

/*********************************************************************/
/* Open a given file as an elf file                                  */
/*********************************************************************/
Elf* OpenElfFile(int *fd, const std::string& file_name)
{
    Elf *elf_file;

    if (( *fd = open(file_name.c_str(), O_RDONLY , 0)) < 0)
    {
        std::cerr << ">> ERROR: " << file_name << " open failed" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (( elf_file = elf_begin(*fd , ELF_C_READ, NULL)) == NULL )
    {
        std::cerr << ">> ERROR: libelf: elf begin failed" << std::endl;
        exit(EXIT_FAILURE);
    }

    return elf_file;
}

/*********************************************************************/
/* Close an open elf file                                            */
/*********************************************************************/
void CloseElfFile(int *fd, Elf *elf_file)
{
    elf_end(elf_file);
    close(*fd);
}

/*********************************************************************/
/* Check if given file is an elf object file or an elf archive       */
/*********************************************************************/
bool IsElf(const std::string& file)
{
    bool status = true;

    int fd;
    Elf *elf_file = OpenElfFile(&fd, file);

    if (elf_kind(elf_file) != ELF_K_ELF && elf_kind(elf_file) != ELF_K_AR)
        status = false;

    CloseElfFile(&fd, elf_file);

    return status;
}

/*********************************************************************/
/* Check if given file is an elf object file                         */
/*********************************************************************/
bool IsElfObj(const std::string& file)
{
    bool status = true;

    int fd;
    Elf *elf_file = OpenElfFile(&fd, file);

    if (elf_kind(elf_file) != ELF_K_ELF && elf_kind(elf_file) == ELF_K_AR)
        status = false;

    CloseElfFile(&fd, elf_file);

    return status;
}

/*********************************************************************/
/* Check if given file is an ELF library created by CLACC            */
/*********************************************************************/
bool IsClaccLib(const std::string& file)
{
    int fd;
    Elf *elf_file, *elf_ar_member;
    Elf_Arhdr *elf_ar_member_hdr;
    GElf_Ehdr elf_hdr;
    bool has_cl     = false;
    bool has_obj    = false;
    bool has_o      = false;

    elf_file = OpenElfFile(&fd, file);

    if (elf_kind(elf_file) == ELF_K_ELF)
    {
        /* If it is an elf object file, then get its not a clacc lib */
        CloseElfFile(&fd, elf_file);
        return false;

    }else if (elf_kind(elf_file) == ELF_K_AR)
    {
        /* If it is an elf archive, then go through the contents and
         * check if it has
         * 1) One .cl file
         * 2) One or more .o files
         * 3) One or more .obj files
         * Return true if all of the above are true.
         * */
        while (( elf_ar_member = elf_begin(fd , ELF_C_READ, elf_file)) != NULL )
        {
            /* Get elf archive header of an archive member */
            if ((elf_ar_member_hdr = elf_getarhdr(elf_ar_member)) == NULL)
            {
                std::cerr << ">> ERROR: libelf: elf getarhdr failed" << std::endl;
                exit(EXIT_FAILURE);
            }

            /* If the ar member is not empty or is not a symbol table
             * then process it */
            std::string member = std::string(elf_ar_member_hdr->ar_name);
            if (member != "/" && member != "//" && member != "__.SYMDEF")
            {
                std::string extension = fs::path(member).extension().string();
                if (extension == ".cl")
                    has_cl = true;
                else
                {
                    /* Get elf header of the archive member */
                    if (gelf_getehdr(elf_ar_member, &elf_hdr) == NULL)
                    {
                        std::cerr << ">> ERROR: libelf: gelf getehdr failed"
                                  << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    if (elf_hdr.e_machine == EM_TI_C6000)
                        has_obj = true;
                    else if(elf_hdr.e_machine == EM_ARM)
                        has_o = true;
                }
            }

            if (has_cl && has_obj && has_o)
                break;

            elf_next(elf_ar_member);
            elf_end(elf_ar_member);
        }
    }

    CloseElfFile(&fd, elf_file);

    if (has_cl && has_obj && has_o)
        return true;

    return false;
}

/*********************************************************************/
/* Get the machine id of the given elf file                          */
/* If file is an archive, then get machine id of first archive member*/
/*********************************************************************/
int GetElfMachineID(const std::string& file)
{
    int fd;
    Elf *elf_file, *elf_ar_member;
    Elf_Arhdr *elf_ar_member_hdr;
    GElf_Ehdr elf_hdr;
    int machine_id = EM_NONE;
    bool got_machine_id = false;

    elf_file = OpenElfFile(&fd, file);

    if (elf_kind(elf_file) == ELF_K_ELF)
    {
        /* If it is an elf object file, then get its header */
        if (gelf_getehdr(elf_file, &elf_hdr) == NULL)
        {
            std::cerr << ">> ERROR: libelf: gelf getehdr failed" << std::endl;
            exit(EXIT_FAILURE);
        }
        machine_id = elf_hdr.e_machine;

    }else if (elf_kind(elf_file) == ELF_K_AR)
    {

        while (!got_machine_id)
        {
            /* If it is an archive, then inspect an archive member */
            if (( elf_ar_member = elf_begin(fd , ELF_C_READ, elf_file)) == NULL )
            {
                std::cerr << ">> ERROR: libelf: elf begin failed" << std::endl;
                exit(EXIT_FAILURE);
            }
            /* Get elf archive header of the archive member */
            if ((elf_ar_member_hdr = elf_getarhdr(elf_ar_member)) == NULL)
            {
                std::cerr << ">> ERROR: libelf: elf getarhdr failed" << std::endl;
                exit(EXIT_FAILURE);
            }

            /* Check if archive member is not empty or is not a symbol table
             * If it is, go to process the next member */
            std::string elf_ar_name = std::string(elf_ar_member_hdr->ar_name);
            if (elf_ar_name == "/"
                || elf_ar_name == "//"
                || elf_ar_name == "__.SYMDEF")
            {
                elf_next(elf_ar_member);
                elf_end(elf_ar_member);
                continue;
            }

            /* Get elf header of the archive member */
            if (gelf_getehdr(elf_ar_member, &elf_hdr) == NULL)
            {
                std::cerr << ">> ERROR: libelf: gelf getehdr failed" << std::endl;
                exit(EXIT_FAILURE);
            }
            /* Get machine ID of first archive member
             * ASSUMPTION: This is not a mixed archive and all members have
             *             the same machine ID */
            machine_id = elf_hdr.e_machine;
            elf_end(elf_ar_member);
            got_machine_id = true;
        }
    }

    CloseElfFile(&fd, elf_file);

    return machine_id;
}

/*********************************************************************/
/* Extract the given Clacc library archive in a temporary folder     */
/* and return the filesystem path to the folder                      */
/*********************************************************************/
fs::path ExtractClaccLib(const std::string& file)
{
    /* Get the path name /tmp/<name of file> */
    std::string tmp_name = fs::temp_directory_path().string()
                           + "/"
                           + fs::path(file).stem().string();
    /* Add 10 random hexadecimal characters at the end of the path
     * End result is similar to /tmp/<name of file>123abcd321 */
    fs::path tmp_dir = fs::unique_path(fs::path(tmp_name + "%%%%%%%%%%"));

    /* Remove path if it exists. In case there is a random number
     * generation conflict from a previous run, this is useful */
    if (fs::exists(tmp_dir))
        fs::remove(tmp_dir);

    /* Create path and extract contents in it */
    if(fs::create_directory(tmp_dir))
    {
        /* Copy library to tmp_dir */
        fs::copy_file(fs::path(file),
                      fs::path(tmp_dir.string() + "/" + file),
                      fs::copy_option::overwrite_if_exists
                      );

        /* Switch to tmp_dir to extract archive */
        fs::path working_dir = fs::current_path();
        current_path(tmp_dir);

        /* Invoke the archiver */
        std::string exec_str = archiver + " x " + file;
        Syscall(exec_str, false);

        /* Remove the copied library */
        fs::remove(fs::path(file));

        /* Switch back to working dir */
        current_path(working_dir);

    }else
    {
        std::cerr << ">> ERROR: Could not create " << tmp_dir
                  << std::endl;
        exit(EXIT_FAILURE);
    }

    return tmp_dir;
}

/*********************************************************************/
/* Get a list of object files of given elf machine id in the given   */
/* filesystem directory                                              */
/*********************************************************************/
std::vector<std::string> GetListOfElfObjs(fs::path dir, int elf_id)
{
    std::vector<std::string> matching_files;
    /* Find matching files in dir and add them to list */
    for(fs::directory_iterator it(dir); it!=fs::directory_iterator(); it++)
    {
        if (IsElf(it->path().string()))
        {
            if (GetElfMachineID(it->path().string()) == elf_id)
                matching_files.push_back(it->path().string());
        }
    }
    return matching_files;
}
