/******************************************************************************
 * Copyright (c) 2019, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 * \file clacc.cc
 * \brief Compiler for OpenMP 4.0 Accelerator Model programs
 */

#define CLACC_USE_ONLY
#include "clacc.h"

/*********************************************************************/
/* Main                                                              */
/*********************************************************************/
int main(int argc, char* argv[])
{
    /*********************************************************************/
    /* Setup option parser and and print help if required                */
    /*********************************************************************/
    po::options_description help_args = SetupCommandLineParser(argc, argv);

    /*********************************************************************/
    /* Initialize compiler options                                       */
    /*********************************************************************/
    InitCompilerOpts();

    /*********************************************************************/
    /* Handle all command line options                                   */
    /*********************************************************************/
    HandleCommandLineOpts();

    std::vector<std::string> input_c_files;
    std::vector<std::string> host_int_files;
    std::vector<std::string> clocl_cl_chain_list;
    std::vector<std::string> generated_tmp_files;

    try
    {
        /*********************************************************************/
        /* Check input file existence and separate object files              */
        /*********************************************************************/
        CheckAndSeparateInputFiles(input_c_files);

        /*********************************************************************/
        /* Initialize compilers                                              */
        /*********************************************************************/
        Compiler h_cc(host_cc, HOST ,debug);
        Compiler h_link_cc(host_link_cc, HOST ,debug);
        Compiler t_cc(target_cc, TARGET ,debug);
        Compiler s2s(target_cc, S2S ,debug);
        Compiler clocl(cl_link_cc, TARGET ,debug);

        /*********************************************************************/
        /* Set compiler options                                              */
        /*********************************************************************/
        /* Even if hc may be empty, we call set_compile_opts so that
         * h_cc and h_link_cc is Setup() */
        std::vector<std::string> hc_opts;
        if (hc.size()!=0)
            hc_opts.push_back(hc);
        /* If hc is not given by user, a WARNING will be given */
        h_cc.set_compile_opts(hc_opts);
        h_link_cc.set_compile_opts(hc_opts);
        h_link_cc.set_link_lib_dependencies(hl);
        t_cc.set_compile_opts(tc);
        if (tl.size()!=0)
        {
            std::vector<std::string> tl_opts;
            tl_opts.push_back(tl);
            t_cc.set_link_lib_dependencies(tl_opts);
        }
        s2s.set_compile_opts(s2s_opts);
        clocl.set_compile_opts(clocl_opts);

        /*********************************************************************/
        /* S2S lowering stage                                                */
        /*********************************************************************/
        DoS2SLowering(s2s,
                      input_c_files,
                      clocl_cl_chain_list,
                      host_int_files,
                      generated_tmp_files
                      );

        /*********************************************************************/
        /* If no target regions exist, .cl files were not generated by the   */
        /* s2s and full_host_execution is set to true to signal a host only  */
        /* build. Make target objects and clocl artifacts only if            */
        /* full_host_execution is turned off                                 */
        /*********************************************************************/
        if (!full_host_execution)
        {
            /*****************************************************************/
            /* Target compile stage                                          */
            /*****************************************************************/
            MakeTargetObjs(t_cc,
                           input_c_files,
                           generated_tmp_files
                           );

            /*****************************************************************/
            /* CLOCL stage                                                   */
            /*****************************************************************/
            MakeCloclArtifacts(clocl,
                               h_link_cc,
                               clocl_cl_chain_list,
                               generated_tmp_files
                               );
        }

        if (making_library)
        {
            /*****************************************************************/
            /* Make Clacc library                                            */
            /*****************************************************************/
            MakeClaccLibrary(h_cc,
                             host_int_files,
                             generated_tmp_files
                             );
        }else
        {
            /*****************************************************************/
            /* Final link stage                                              */
            /*****************************************************************/
            DoFinalLink(h_cc,
                        h_link_cc,
                        host_int_files,
                        generated_tmp_files
                        );
        }
        /*********************************************************************/
        /* Clean-up stage                                                    */
        /*********************************************************************/
        CleanUp(generated_tmp_files);

    }
    catch(fs::filesystem_error& e)
    {
        std::cout << ">> ERROR: Filesystem: " << e.what() << std::endl;
        return 1;
    }
    catch(std::exception& e)
    {
        std::cout << e.what() << std::endl;
        return 1;
    }

    return 0;
}

/*********************************************************************/
/* Print Usage                                                       */
/*********************************************************************/
void PrintUsage(po::options_description& visible)
{
    std::cout << "TI CLACC OpenMP Accelerator Model Compiler"
              << std::endl
              << "Version:    " << clacc_version
              << std::endl
#ifdef BUILD_DATE
              << "Build Date: " << build_date
              << std::endl
#endif
              << "Usage:      clacc [options] [object-files] C-source-files"
              << std::endl
              << "Note:       Environment variables "
              << ti_ocl_cgt_env_var
              << "(TI CGT Install Location), "
              << ti_rootdir_env_var
              << "(TI MCSDK Root Filesystem Location) are accepted"
              << std::endl
              << visible
              << std::endl;
}

/*********************************************************************/
/* Check and get required environment variables                      */
/*********************************************************************/
void GetEnvVars()
{
    /* Use TI_OCL_CGT_INSTALL value if set */
    char* ti_ocl_cgt_install_path =  getenv(ti_ocl_cgt_env_var.c_str());
    if (ti_ocl_cgt_install_path != NULL)
        ti_cgt_path = std::string(ti_ocl_cgt_install_path);

    /* Use TARGET_ROOTDIR value if set */
    char* ti_rootdir_path_cstr =  getenv(ti_rootdir_env_var.c_str());
    if (ti_rootdir_path_cstr != NULL)
        ti_rootdir_path = std::string(ti_rootdir_path_cstr);
}

/*********************************************************************/
/* Initialize Compiler Options for all compilers being used          */
/*********************************************************************/
void InitCompilerOpts()
{
    /* Check and get cgt and target rootdir paths if set */
    GetEnvVars();

    /* Include path for cl6x */
    std::string cgt_include_path = "-I";
    cgt_include_path.append(ti_cgt_path);
    cgt_include_path.append("/include");

    /* Convert upper case CLACC_CC and CLACC_CXX arguments passed in
     * from CMake to lower case. This is done to workaround a gcc bug which
     * converts the lower case word 'linux' to 1. */
    std::transform(host_cc.begin(),
                   host_cc.end(),
                   host_cc.begin(),
                   ::tolower);

    std::transform(host_link_cc.begin(),
                   host_link_cc.end(),
                   host_link_cc.begin(),
                   ::tolower);

    /* Lib path for g++/gcc. OpenCL, OpenMPAcc libs are present here */
    std::string ti_rootdir_lib_path = ti_rootdir_path + "/usr/lib";

    /* Include path for ti_omp_device.h */
    std::string ti_rootdir_inc_path = ti_rootdir_path + "/usr/include";

    /* Include path for cl6x for DSP side omp.h */
    std::string ti_rootdir_dsp_inc_path = ti_rootdir_path + oa_inc_path ;

    /* Lib path prefix for g++/gcc that allows finds OpenCL depency
     * libs such as cmem, hyplink, bfd, sqlite3 etc*/
    std::string ti_rootdir_rpath_prefix = "-Wl,-rpath-link,";

    /* Generate the name of a temp file to hold the host compiler (gcc)
     * predefined symbols */
    char name_out[] = "/tmp/gcc_predefXXXXXX";
    int fd = mkstemp(name_out);
    close(fd);
    gcc_predef_file_name = std::string(name_out);

    clocl_opts = {"-t"};

    hl = {"--sysroot=" + ti_rootdir_path,
          "-lOpenMPAcc",
          "-lOpenCL",
          "-locl_util"
          };

    tc = {"--openmp:device",
          "-mv6600",
          cgt_include_path,
          "-I" + ti_rootdir_dsp_inc_path
          };

    /* -pds=1173 disables: warning: unknown attribute "__nonnull__" */
    s2s_opts = {"--preinclude=" + gcc_predef_file_name,
                "--openmp:host","-pds=1173",
                "-I" + ti_rootdir_inc_path
                };

}

/*********************************************************************/
/* Setup Command Line parser and print help if required              */
/*********************************************************************/
po::options_description SetupCommandLineParser(int argc, char* argv[])
{
    /*********************************************************************/
    /* Declare command line options visible to user                      */
    /*********************************************************************/
    po::options_description visible("Options");
    visible.add_options()
        ("help,h","Show help message")
        ("verbose,v","Show debug output")
        ("keep_files,k", "Keep intermediate temporary files")
        ("runtime_verbose,d", "Enable runtime debugging information display")
        ("runtime_performance,p", "Enable runtime performance information display")
        ("debug,g", "Generate target debug symbols")
        ("make_lib", "Make static library")
        ("host_cc", po::value<std::string>(),
         "Host compiler to use")
        ("hc", po::value<std::string>(),
         "Host compiler options")
        ("tc", po::value<std::string>(),
         "Target compiler options")
        ("hl", po::value<std::string>(),
         "Host linker options")
        ("tl", po::value<std::string>(),
         "Target linker options")
        ("show_cc_opts", "Show host and target compiler options being used")
        ("exe,o", po::value<std::string>(),
         "Name of executable fat binary")
        ;

    /*********************************************************************/
    /* Allowed hidden options, not visible to user                       */
    /*********************************************************************/
    po::options_description hidden("Hidden options");
    hidden.add_options()
        ("input-file", po::value< std::vector<std::string> >(), "input file")
        ("s2s_inc_path", po::value< std::vector<std::string> >()->composing(),
         "Add include path for S2S translator")
        ("host_objs", po::value< std::vector<std::string> >(),
         "Pre-compiled host object files to link")
        ("target_objs", po::value< std::vector<std::string> >(),
         "Pre-compiled target object files to link")
        ;

    try
    {
        po::options_description cmdline_options;
        cmdline_options.add(visible).add(hidden);

        po::positional_options_description p;
        p.add("input-file", -1);

        po::parsed_options parsed = po::command_line_parser(argc, argv)
            .options(cmdline_options)
            .positional(p)
            .run();

        po::store(parsed, vm);
        po::notify(vm);

        /* Check if help required */
        if (vm.count("help"))
        {
            PrintUsage(visible);
            exit(EXIT_SUCCESS);
        }
    }
    catch(...)
    {
        PrintUsage(visible);
        exit(EXIT_FAILURE);
    }

    return visible;
}

/*********************************************************************/
/* Handle command line options                                       */
/*********************************************************************/
void HandleCommandLineOpts()
{
    std::vector<std::string> s2s_inc_paths;

    if (vm.count("verbose"))
    {
        debug = true;
        clocl_opts.push_back("-v");
    }

    if (vm.count("keep_files"))
    {
        keep_tmp_files = true;
    }

    if (vm.count("runtime_verbose"))
    {
        runtime_verbose = true;
    }

    if (vm.count("make_lib"))
    {
        making_library = true;
        if (!vm.count("exe"))
        {
            std::cerr << ">> ERROR: No library name specified."
                      << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    if (vm.count("debug"))
    {
        clocl_opts.push_back("-g");
    }

    if (vm.count("runtime_performance"))
    {
        runtime_performance = true;
    }

    if (vm.count("host_cc"))
    {
        host_cc = vm["host_cc"].as<std::string>();
        if (debug)
        {
            std::cout << ">> DEBUG: Using host compiler: "
                      << host_cc
                      << std::endl;
        }
    }

    if (vm.count("hc"))
    {
        hc  = vm["hc"].as<std::string>();
        if (debug)
        {
            std::cout << ">> DEBUG: Extra host compiler options: "
                      << hc
                      << std::endl;
        }
    }

    if (vm.count("tc"))
    {
        tc_variable = vm["tc"].as<std::string>();

        if (debug)
        {
            std::cout << ">> DEBUG: Extra target compiler options: "
                      << tc_variable
                      << std::endl;
        }
    }

    if (vm.count("hl"))
    {
        hl_variable = vm["hl"].as<std::string>();
        if (debug)
        {
            std::cout << ">> DEBUG: Extra Host linker options: "
                      << hl_variable
                      << std::endl;
        }
    }

    if (vm.count("tl"))
    {
        tl = vm["tl"].as<std::string>();
        if (debug)
        {
            std::cout << ">> DEBUG: Extra Target linker options: "
                      << tl
                      << std::endl;
        }
    }

    if (vm.count("s2s_inc_path"))
    {
        s2s_inc_paths = vm["s2s_inc_path"].as< std::vector<std::string> >();

        if (debug)
        {
            std::cout << ">> DEBUG: S2S include paths added: "
                      << s2s_inc_paths
                      << std::endl;
        }
    }

    /* If the user has specified s2s paths, use those */
    if (s2s_inc_paths.size() > 0)
    {
        /* Add the user specified s2s_inc_paths in order */
        for (auto path: s2s_inc_paths)
        {
            s2s_opts_variable.push_back("-I" + path);
        }
    }else /* Otherwise use the ones from the host cc */
    {
        GetHostIncludePaths();

        /* If successful in getting host cc inc paths, add them */
        if (host_cc_inc_paths.size() > 0)
        {
            for (auto path: host_cc_inc_paths)
            {
                s2s_opts_variable.push_back("-I" + path);
            }
        }else
        {
            /* If not successful, don't quit,
             * ask user to supply paths using s2s_inc_path option */
            std::cout << ">> WARNING: Unable to get standard include "
                      << "paths from " << host_cc << ".\n\t    Please use "
                      << "--s2s_inc_path=\"PATH\" option to specify host "
                      << "include \n\t    paths to be used by s2s translator."
                      << std::endl;
        }

    }

    /* Compose host linker options */
    /* Add user specified options to beginning of hl, before the fixed opts */
    if (hl_variable.size()!=0)
    {
        hl.insert(hl.begin(), hl_variable);
    }

    /* Create tc and s2s_opts as variable opts have now been specified */
    /* Add target compiler options */
    if (tc_variable.size()!=0)
    {
        tc.push_back(tc_variable);
        s2s_opts.push_back(tc_variable);
    }

    /* S2S variable opts may not have been set, so check for it first*/
    if (s2s_opts_variable.size()!=0)
    {
        s2s_opts.insert(s2s_opts.end(),
                        s2s_opts_variable.begin(),
                        s2s_opts_variable.end()
                        );
    }

    if (vm.count("show_cc_opts"))
    {
        std::cout << ">> Host CC: " << host_cc
                  << " " << hc
                  << std::endl;
        std::cout << ">> Target CC: " << target_cc
                  << " " << tc
                  << std::endl;
        std::cout << ">> S2S: " << target_cc
                  << " " << s2s_opts
                  << std::endl;
    }

    if (vm.count("exe"))
    {
        exe_name = vm["exe"].as<std::string>();
        if (debug)
        {
            std::cout << ">> DEBUG: "
                      << (vm.count("make_lib") ? "Library" : "Executable binary")
                      << " name: " << exe_name
                      << std::endl;
        }
    }

    if (vm.count("host_objs"))
    {
        user_host_objs = vm["host_objs"].as< std::vector<std::string> >();
        host_objs = user_host_objs;
        if (debug)
        {
            std::cout << ">> DEBUG: Pre-compiled host objs: "
                      << host_objs
                      << std::endl;
        }
    }

    if (vm.count("target_objs"))
    {
        target_objs = vm["target_objs"].as< std::vector<std::string> >();
        if (debug)
        {
            std::cout << ">> DEBUG: Pre-compiled target objs: "
                      << target_objs
                      << std::endl;
        }
    }

    if (vm.count("input-file"))
    {
        input_files = vm["input-file"].as< std::vector<std::string> >();
        if (debug)
        {
            std::cout << ">> DEBUG: Input files: " << input_files
                      << std::endl;
        }
    }else
    {
        std::cerr << ">> ERROR: No input files"
                  << std::endl;
        exit(EXIT_FAILURE);
    }

}

/*********************************************************************/
/* Check each given input file for existence and separate them       */
/* into lots of C source files and ARM host and C66 target           */
/* object files                                                      */
/*********************************************************************/
void CheckAndSeparateInputFiles(std::vector<std::string>& input_c_files)
{
    ElfInit();
    for (auto infile : input_files)
    {
        if(!fs::exists(fs::path(infile)))
        {
            std::cerr << ">> ERROR: File " << infile
                      << " not found"
                      << std::endl;
            exit(EXIT_FAILURE);
        }

        /* If it is a source code file */
        if(!IsElf(infile))
        {
            if (debug)
            {
                std::cout << ">> DEBUG: Source code file: " << infile
                          << std::endl;
            }
            input_c_files.push_back(infile);
        }
        else
        {
            if (IsClaccLib(infile))
            {
                if (debug)
                {
                   std::cout << ">> DEBUG: CLACC library archive: "
                             << infile
                             << std::endl;
                }

                extracted_clacc_lib_dirs.push_back(ExtractClaccLib(infile));
                continue;
            }
            /* It is an ELF object file */
            switch(GetElfMachineID(infile))
            {
                case EM_ARM:
                    {
                        if (debug)
                        {
                            if (!making_library ||
                                (making_library && IsElfObj(infile)))
                            {
                                std::cout << ">> DEBUG: "
                                          << "ARM object file or archive: "
                                          << infile
                                          << std::endl;
                            }
                        }
                        /* Include this elf file if not making a library OR
                         * include it if making a library and it is an
                         * elf object file and not an elf archive*/
                        if (!making_library ||
                            (making_library && IsElfObj(infile)))
                        {
                            host_objs.push_back(infile);
                            user_host_objs.push_back(infile);
                        }
                        break;
                    }
                case EM_TI_C6000:
                    {
                        if (debug)
                        {
                            if (!making_library ||
                                (making_library && IsElfObj(infile)))
                            {
                                std::cout << ">> DEBUG: "
                                          << "TI C66 DSP object file or archive: "
                                          << infile
                                          << std::endl;
                            }
                        }
                        /* Include this elf file if not making a library OR
                         * include it if making a library and it is an
                         * elf object file and not an elf archive*/
                        if (!making_library ||
                            (making_library && IsElfObj(infile)))
                        {
                            target_objs.push_back(infile);
                        }
                        break;
                    }
                default:
                    {
                        std::cerr << ">> ERROR: Unsupported ELF Object "
                                  << infile
                                  << std::endl;
                        exit(EXIT_FAILURE);
                    }
            }
        }
    }
}

/*********************************************************************/
/* Create object files for target                                    */
/*********************************************************************/
void MakeTargetObjs(Compiler& t_cc,
                    std::vector<std::string>& input_c_files,
                    std::vector<std::string>& tmp_list)
{
    /* Create .obj files  */
    std::string obj_file;
    for(auto infile : input_c_files)
    {
        /* Generate .obj files for each input file */
        t_cc.Compile(infile);
        obj_file = fs::path(infile).stem().string();
        /* Check existence and add the generated obj file name
         * to target obj list
         * */
        obj_file.append(".obj");
        if (fs::exists(fs::path(obj_file)))
        {
            target_objs.push_back(obj_file);
            tmp_list.push_back(obj_file);
        }else
        {
            /* Check if last compilation was successful */
            if (!t_cc.Pass())
            {
                /* If non-zero, compile was not successful, exit */
                std::cout << ">> ERROR: Target compilation of "
                          << infile << " failed."
                          << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }

    /* If any CLACC libs were provided, get the target objs from them */
    if (extracted_clacc_lib_dirs.size() != 0 && !making_library)
    {
        for (auto& dir : extracted_clacc_lib_dirs)
        {
            std::vector<std::string> lib_objs = GetListOfElfObjs(dir,
                                                                 EM_TI_C6000
                                                                 );
            target_objs.insert(target_objs.end(),
                               lib_objs.begin(),
                               lib_objs.end()
                               );
        }
    }
}

/*********************************************************************/
/* Perform S2S Lowering to create host int.c and .cl files           */
/*********************************************************************/
void DoS2SLowering(Compiler& s2s,
                   std::vector<std::string>& input_c_files,
                   std::vector<std::string>& cl_chain,
                   std::vector<std::string>& h_int_list,
                   std::vector<std::string>& tmp_list)
{
    int s2s_fail_count = 0;
    /* Create .int.c and .cl files */
    std::string cl_file;
    std::string int_file;

    GetHostPreDefMacros(tmp_list);

    for(auto infile : input_c_files)
    {
        /* Generate .int.c and .cl files */
        s2s.Compile(infile);
        if (!s2s.Pass())
            ++s2s_fail_count;

        cl_file = fs::path(infile).stem().string();
        int_file = cl_file;
        /* ASSUMPTION: At this stage the files that do not have
         * target regions will not generate .cl files
         * */
        /* Check to see whether .cl file has been generated
         * and add it to clocl chain list
         * */
        cl_file.append(".cl");
        if (fs::exists(fs::path(cl_file)))
        {
            cl_chain.push_back(cl_file);
            tmp_list.push_back(cl_file);
        }
        /* Check existence and add the generated int.c file name
         * to host_int_files list
         * */
        int_file.append(".int.c");
        if(fs::exists(fs::path(int_file)))
        {
            h_int_list.push_back(int_file);
            tmp_list.push_back(int_file);
        }
    }

    /* If CLACC libs were provided, add their cl files to chain */
    if (extracted_clacc_lib_dirs.size() != 0 && !making_library)
    {
        for (auto& dir : extracted_clacc_lib_dirs)
        {
            std::vector<std::string> lib_cl_files = GetListOfFilesOfType(dir,
                                                                         ".cl"
                                                                         );
            cl_chain.insert(cl_chain.end(),
                            lib_cl_files.begin(),
                            lib_cl_files.end()
                            );
        }
    }

    /* If no target regions exist in the code, no .cl files are generated
     * In this case, generate host only executable if s2s didn't return
     * a non-zero value at any point
     * */
    if (cl_chain.size() == 0)
    {
        if (s2s_fail_count != 0)
        {
            std::cout << ">> ERROR: Failed to compile target code."
                      << std::endl;
            exit(EXIT_FAILURE);
        }else
        {
            full_host_execution = true;
        }
    }
}

/*********************************************************************/
/* Chain all generated .cl files together (if present) and           */
/* run CLOCL on the chained cl file with the target object files to  */
/* create a header file with a char array containing the cl program  */
/* binary. Encode the lenght of the char array and the array in a C  */
/* file and compile the c file to an object file for final stage link*/
/*********************************************************************/
void MakeCloclArtifacts(Compiler& clocl,
                        Compiler& h_link_cc,
                        std::vector<std::string>& cl_chain,
                        std::vector<std::string>& tmp_list
                        )
{
    /* Chain together all .cl files into one .cl file for clocl to compile */
    /* If making library, then the library name is used to create the
     * chained cl file */
    if (making_library)
        chained_cl_file_name = fs::path(exe_name).stem().string() + ".cl";

    std::string cl_filename_stem =
        fs::path(chained_cl_file_name).stem().string();

    std::ofstream chained_cl_file_stream(chained_cl_file_name,
                                         std::ios_base::binary
                                         );
    for(auto clfile : cl_chain)
    {
        std::ifstream cl_file_stream(clfile, std::ios_base::binary);
        chained_cl_file_stream << cl_file_stream.rdbuf();
        cl_file_stream.close();
    }
    chained_cl_file_stream.close();
    tmp_list.push_back(chained_cl_file_name);

    if (!making_library)
    {
        /* CLOCL: link DSP objs to create binary header file */
        clocl.set_link_lib_dependencies(target_objs);
        clocl.Link(chained_cl_file_name);
        if (!clocl.Pass())
        {
            std::cerr << ">> ERROR: Failed to compile "<< chained_cl_file_name
                      << std::endl;
            exit(EXIT_FAILURE);
        }

        /* CLOCL generates a .out file by default */
        /* Check if it exists and remove this if -k is not specified */
        std::string clocl_cl_out_file = cl_filename_stem + ".out";
        if (fs::exists(fs::path(clocl_cl_out_file)))
        {
            tmp_list.push_back(clocl_cl_out_file);
        }

        /* Check binary header file existence and add it to
         * final link source files */
        std::string cl_bin_header_file = cl_filename_stem + ".dsp_h";
        std::string cl_bin_c_file = cl_filename_stem + ".c";
        std::string cl_bin_o_file = cl_filename_stem + ".o";
        if (fs::exists(fs::path(cl_bin_header_file)))
        {
            tmp_list.push_back(cl_bin_header_file);
            /* Create the C file */
            fs::copy_file(fs::path(cl_bin_header_file),
                          fs::path(cl_bin_c_file),
                          fs::copy_option::overwrite_if_exists
                          );

            if (fs::exists(fs::path(cl_bin_c_file)))
            {
                tmp_list.push_back(cl_bin_c_file);
                /* Append the debug variables to the C file */
                std::string runtime_dbg_str = runtime_dbg_var + " = ";
                std::string runtime_perf_str = runtime_perf_var + " = ";
                std::ofstream cl_bin_c_file_ostream (cl_bin_c_file,
                                                     std::ios_base::app
                                                     | std::ios_base::out
                                                     );
                /* Check if runtime debugging info is enabled and
                 * append hook for it */
                cl_bin_c_file_ostream << "bool " << runtime_dbg_str;
                if (runtime_verbose)
                {
                    cl_bin_c_file_ostream << "true";
                }else
                {
                    cl_bin_c_file_ostream << "false";
                }
                cl_bin_c_file_ostream << ";\n";

                /* Check if runtime performance info is enabled and
                 * append hook for it */
                cl_bin_c_file_ostream << "bool " << runtime_perf_str;
                if (runtime_performance)
                {
                    cl_bin_c_file_ostream << "true";
                }else
                {
                    cl_bin_c_file_ostream << "false";
                }
                cl_bin_c_file_ostream << ";\n";

                /* Done appending. Close the file stream */
                cl_bin_c_file_ostream.close();

                /* Compile C file with host link cc compiler */
                h_link_cc.Compile(cl_bin_c_file);
                if (fs::exists(fs::path(cl_bin_o_file)))
                {
                    /* Add compiled .o file to final link objs */
                    host_objs.push_back(cl_bin_o_file);
                    tmp_list.push_back(cl_bin_o_file);
                }else
                {
                    std::cerr << ">> ERROR: " << cl_bin_o_file
                              << " not generated."
                              << std::endl;
                    exit(EXIT_FAILURE);
                }
            }else
            {
                std::cerr << ">> ERROR: " << cl_bin_c_file
                          << " not generated."
                          << std::endl;
                exit(EXIT_FAILURE);
            }
        }else
        {
            std::cerr << ">> ERROR: " << cl_bin_header_file
                      << " not generated."
                      << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

/*********************************************************************/
/* Create library with a single .cl, all .obj's and all .o's         */
/*********************************************************************/
void MakeClaccLibrary(Compiler& h_cc,
                      std::vector<std::string>& h_int_files,
                      std::vector<std::string>& tmp_list)
{
    if (full_host_execution)
    {
        std::cout << ">> ERROR: No target regions were found in source files."
                  << "          Library not created."
                  << std::endl;
        exit(EXIT_FAILURE);
    }
    /* Compile lowered int.c files */
    std::string int_o_file;
    for (auto int_file : h_int_files)
    {
        h_cc.Compile(int_file);
        if (!h_cc.Pass())
        {
            std::cerr << ">> ERROR: Failed to compile "<< int_file
                << std::endl;
            exit(EXIT_FAILURE);
        }
        int_o_file = fs::path(int_file).stem().string();
        int_o_file.append(".o");
        if(fs::exists(fs::path(int_o_file)))
        {
            host_objs.push_back(int_o_file);
            tmp_list.push_back(int_o_file);
        }
    }

    /* Make list of all files to package in library */
    std::vector<std::string> lib_files;
    lib_files.push_back(chained_cl_file_name);
    lib_files.insert(lib_files.end(),
                     host_objs.begin(),
                     host_objs.end()
                     );
    lib_files.insert(lib_files.end(),
                     target_objs.begin(),
                     target_objs.end()
                     );
    /* Create library */
    std::string exec_str = archiver + " rcs "
                           + exe_name + " "
                           + GetStringOfElements(lib_files, " ");
    Syscall(exec_str, debug);

}

/*********************************************************************/
/* Perform final linking into fat binary                             */
/* If no cl files were generated then full host execution is enabled */
/* In this case generate ARM only executable                         */
/*********************************************************************/
void DoFinalLink(Compiler& h_cc,
                 Compiler& h_link_cc,
                 std::vector<std::string>& h_int_files,
                 std::vector<std::string>& tmp_list)
{
    /* Link all .int.o files with TIOpenMPACC lib and OpenCL lib with DSP
     * binary header to create fat binary */
    /* If no target regions present, i.e. no .cl files have been generated,
     * create host binary only or else create fat binary with target code */
    if (full_host_execution)
    {
        std::cout << ">> WARNING: No target regions were found in source files."
                  << std::endl
                  << ">>          Generating ARM host only executable."
                  << std::endl;
        /* Compile lowered int.c files */
        std::string int_o_file;
        /* Check if any int.c files were generated by s2s */
        if (h_int_files.size() > 0)
        {
            /* Compile them to .o files */
            for (auto int_file : h_int_files)
            {
                h_cc.Compile(int_file);
                if (!h_cc.Pass())
                {
                    std::cerr << ">> ERROR: Failed to compile "<< int_file
                              << std::endl;
                    exit(EXIT_FAILURE);
                }
                int_o_file = fs::path(int_file).stem().string();
                int_o_file.append(".o");
                if(fs::exists(fs::path(int_o_file)))
                {
                    user_host_objs.push_back(int_o_file);
                    tmp_list.push_back(int_o_file);
                }
            }
        }
        else /* Quit if there are no int.c files present */
        {
            std::cout << ">> ERROR: No int.c files produced by s2s translation"
                      << std::endl;
            exit(EXIT_FAILURE);
        }

        h_link_cc.Link(user_host_objs, exe_name);
        if (!h_link_cc.Pass())
        {
            std::cerr << ">> ERROR: Failed to link "<< user_host_objs
                      << std::endl;
            exit(EXIT_FAILURE);
        }
    }else
    {
        /* Compile lowered int.c files */
        std::string int_o_file;
        for (auto int_file : h_int_files)
        {
            h_cc.Compile(int_file);
            if (!h_cc.Pass())
            {
                std::cerr << ">> ERROR: Failed to compile "<< int_file
                          << std::endl;
                exit(EXIT_FAILURE);
            }
            int_o_file = fs::path(int_file).stem().string();
            int_o_file.append(".o");
            if(fs::exists(fs::path(int_o_file)))
            {
                host_objs.push_back(int_o_file);
                tmp_list.push_back(int_o_file);
            }
        }
        /* If CLACC libs were provided, add .o files from them to final link */
        if (extracted_clacc_lib_dirs.size() != 0 && !making_library)
        {
            for (auto& dir : extracted_clacc_lib_dirs)
            {
                std::vector<std::string> lib_o_files = GetListOfElfObjs(dir,
                                                                        EM_ARM
                                                                        );
                host_objs.insert(host_objs.end(),
                                 lib_o_files.begin(),
                                 lib_o_files.end()
                                 );
            }
        }
        /* If no host objs were produced, then there must
         * have been errors in compiling int.c files */
        if (user_host_objs.size() == host_objs.size())
        {
            std::cerr << ">> ERROR: Compilation of host objects failed"
                      << std::endl;
            exit(EXIT_FAILURE);
        }

        h_link_cc.Link(host_objs, exe_name);
        if (!h_link_cc.Pass())
        {
            std::cerr << ">> ERROR: Failed to link "<< host_objs
                      << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

/*********************************************************************/
/* Unless -k is set, remove all intermediate files and if debug is   */
/* set then report if the fat binary was created or not              */
/*********************************************************************/
void CleanUp(std::vector<std::string>& tmp_list)
{
    /* Remove intermediate files unless -v specified */
    if(!keep_tmp_files)
    {
        for (auto& tmp_file : tmp_list)
        {
            fs::remove(fs::path(tmp_file));
        }
    }

    /* Remove extracted CLACC lib directories */
    if (extracted_clacc_lib_dirs.size() != 0)
    {
        for (auto& dir : extracted_clacc_lib_dirs)
        {
            fs::remove_all(dir);
        }
    }

    if (debug)
    {
        if(fs::exists(fs::path(exe_name)))
            std::cout << ">> DEBUG: "
                      << (making_library ? "CLACC library ":"Executable binary ")
                      << exe_name << " created successfully."
                      << std::endl;
        else
            std::cerr << ">> ERROR: Fat binary not created."
                      << std::endl;
    }
}

/*********************************************************************/
/* Get include paths from host compiler to be used with s2s          */
/*********************************************************************/
void GetHostIncludePaths()
{
    std::string command = host_cc + " -Wp,-v -E - </dev/null 2>&1";
    std::string line;
    char line_buf[4096];
    FILE* pipe;

    /* Open a pipe to get the output of the command */
    if(!(pipe = popen(command.c_str(), "r")))
        return;

    while(fgets(line_buf, sizeof(line_buf), pipe)!=NULL)
    {
        /* If a path is > 4096 characters, then return silently */
        if (line_buf[strlen(line_buf)-1] != '\n')
        {
            pclose(pipe);
            return;
        }

        /* Get the next line */
        line = std::string(line_buf);
        /* Trim all whitespace from both ends */
        algo::trim(line);
        /* If the first character is a back-slash, then it is a path */
        if (line[0] == '/')
        {
            /* Add it to list */
            host_cc_inc_paths.push_back(line);
        }
    }
    /* Close the pipe */
    pclose(pipe);
}

/*********************************************************************/
/* Get predefined macros from host compiler to be used by s2s        */
/*********************************************************************/
static void GetHostPreDefMacros(std::vector<std::string>& tmp_list)
{
    std::string command = host_cc + " -dM -E - < /dev/null 2>&1";
    std::string line;
    char line_buf[4096];
    FILE* pipe;

    /* Open a pipe to get the output of the command */
    if(!(pipe = popen(command.c_str(), "r")))
        return;

    tmp_list.push_back(gcc_predef_file_name);
    FILE *fp = fopen(gcc_predef_file_name.c_str(), "w");
    if (!fp)
    {
        pclose(pipe);
        return;
    }

    /* Before cl6x compiler version 8.1.2, the parser
     * harcoded the gcc predfined macros.  */
    fputs("/* gcc -dM -E - < /dev/null */\n", fp);
    fputs("#if __TI_COMPILER_VERSION__ > 8001001\n", fp);

    while(fgets(line_buf, sizeof(line_buf), pipe)!=NULL)
    {
#if 0
        if ((strstr(line_buf, " __VERSION__ ") == NULL) &&
            (strstr(line_buf, " __STDC_VERSION__ ") == NULL))
#endif
        fputs(line_buf, fp);
    }

    /* Silence a warning about in cl6x compiler version pre 8.1.2 */
    fputs("#else\n", fp);
    fputs("#undef _FORTIFY_SOURCE\n", fp);
    fputs("#endif\n", fp);

    /* Close the pipe */
    pclose(pipe);
    fclose(fp);
}
