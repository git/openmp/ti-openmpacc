/******************************************************************************
 * Copyright (c) 2019, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 * \file util.h
 * \brief Utility functions used within CLACC
 */

#ifndef UTIL_H_
#define UTIL_H_

#define BOOST_NO_SCOPED_ENUMS
#define BOOST_NO_CXX11_SCOPED_ENUMS

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iterator>
#include <cstdio>
#include <vector>
#include <boost/filesystem.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

namespace fs    = boost::filesystem;
namespace po    = boost::program_options;
namespace algo  = boost::algorithm;

/**
 * \brief Pretty print vectors to a stream
 * \param os Output stream reference
 * \param v  Vector of any type
 * \return Reference to output stream object
 */
template<class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
    copy(v.begin(), v.end(), std::ostream_iterator<T>(os, " "));
    return os;
}

/**
 * \brief Get a concatenated string of elements provided
 * \param list         Reference to vector of string elements
 *                     to concatenate
 * \param conjunction  String to place in-between two elements
 * \return Concatenated string of elements provided
 */
inline std::string GetStringOfElements(std::vector<std::string>& list,
                                        std::string conjunction)
{
    std::string elements;
    for(auto element : list)
    {
        elements.append(element);
        elements.append(conjunction);
    }

    return elements;
}

/**
 * \brief Execute a command on the system
 * \param syscall_string  Reference to comand string
 * \param debug           Enable/disable printing the command
 * \return Status of system() call
 */
inline int Syscall(std::string& syscall_string, bool debug)
{
    if (debug)
        std::cout << syscall_string << std::endl;
    return system((const char*) syscall_string.c_str());
}

/**
 * \brief Get a list of files in the given directory with the
 *        given file type extension
 * \param dir   Filesystem directory to look in
 * \param type  File extension type
 * \return Vector of filesystem paths of matching files
 */
inline std::vector<std::string> GetListOfFilesOfType(fs::path dir,
                                                     std::string type)
{
    std::vector<std::string> matching_files;
    /* Find matching files in dir and add them to list */
    for(fs::directory_iterator it(dir); it!=fs::directory_iterator(); it++)
    {
        if (it->path().extension().string() == type)
        {
           matching_files.push_back(it->path().string());
        }
    }
    return matching_files;
}

#endif
