/******************************************************************************
 * Copyright (c) 2019, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 * \file compiler.h
 * \brief Host, Target, S2S Compiler abstraction for CLACC
 */

#ifndef COMPILER_H_
#define COMPILER_H_

#include "util.h"
#include <map>

/**
 * \enum CompilerType
 * \brief Type of compiler being used. ARM Host, DSP Target or
 *        source to source translator
 */
enum CompilerType
{
    HOST,
    TARGET,
    S2S,
};

/*********************************************************************/
/* String map for CompilerType enums                                 */
/*********************************************************************/
static std::map<CompilerType, std::string> CompilerTypeString =
            boost::assign::map_list_of(HOST,"HOST")(TARGET,"TARGET")(S2S,"S2S");

/**
 * \class Compiler
 * \brief Compiler instances used by CLACC
 *
 * This class builds an instance of a compiler used by CLACC to either
 * perform ARM host side compilation/linkage or DSP target side
 * compilation or OpenMP target region source to source lowering
 */
class Compiler
{
    public:
        /**
         * \brief Constructor
         * \param name  Name of compiler instance
         * \param type  Type of compiler i.e. CompilerType
         * \param debug Enable/disable printing of debugging information
         */
        Compiler(std::string name, CompilerType type, bool debug);

        /**
         * \brief Destructor
         */
        ~Compiler(){}

        /*********************************************************************/
        /* Accessors                                                         */
        /*********************************************************************/
        const std::string& name()
        { return name_; }
        const CompilerType& type()
        { return type_; }
        const std::vector<std::string>& compile_opts()
        { return compile_opts_; }
        const std::vector<std::string>& link_lib_dependencies()
        { return link_lib_dependencies_; }

        /*********************************************************************/
        /* Mutators                                                          */
        /*********************************************************************/
        void set_name(std::string& name)
        { name_ = name; }
        void set_type(CompilerType type)
        { type_ = type; }
        void set_compile_opts(std::vector<std::string>& compile_opts)
        { compile_opts_ = compile_opts; Setup(); }
        void set_link_lib_dependencies
        (std::vector<std::string>& link_lib_dependencies)
        { link_lib_dependencies_ = link_lib_dependencies; }

        /**
         * \brief   Check status of last compilation
         * \return  If last compilation was successful i.e. returned 0
         *          then return true, else false
         */
        bool Pass()
        {
            if (exec_return_value_ == 0) return true;
            else                         return false;
        }

        /**
         * \brief Compile file to obj file
         * \param file Name of file to compile
         */
        void Compile(std::string& file);

        /**
         * \brief Compile files to obj files
         * \param files List names of files to compile
         */
        void Compile(std::vector<std::string>& files);

        /**
         * \brief Compile and Link to make a.out
         * \param file      Name of file to compile and link
         * \param link_libs Enable/Disable linking against
         *                  preconfigured libraries
         */
        void Link(std::string& file, bool link_libs = true);

        /**
         * \brief Compile and Link to make a.out
         * \param files     List of names of files to compile and link
         * \param link_libs Enable/Disable linking against
         *                  preconfigured libraries
         */
        void Link(std::vector<std::string>& files, bool link_libs = true);

        /* */

        /**
         * \brief Compile and Link to make exe_name executable
         * \param file      Name of file to compile and link
         * \param exe_name  Name of executable to create
         * \param link_libs Enable/Disable linking against
         *                  preconfigured libraries
         */
        void Link(std::string& file,
                  std::string& exe_name,
                  bool link_libs = true
                  );

        /**
         * \brief Compile and Link to make exe_name executable
         * \param files     List of names of files to compile and link
         * \param exe_name  Name of executable to create
         * \param link_libs Enable/Disable linking against
         *                  preconfigured libraries
         */
        void Link(std::vector<std::string>& files,
                  std::string& exe_name,
                  bool link_libs = true
                  );

    private:
        int exec_return_value_;
        std::string name_; /* Name */
        CompilerType type_; /* HOST or TARGET or S2S */
        bool debug_; /* Enable printing debug info */
        bool ready_to_compile_; /* Enable printing debug info */
        std::vector<std::string> compile_opts_; /* Compile flags */
        std::vector<std::string> link_lib_dependencies_; /* Lib dependencies */
        std::string cc_str_; /* cc {cc_opts} */

        /**
         * \brief Get Ready for compilation i.e. setup cc_str_
         */
        void Setup();

        /**
         * \brief Execute a system call
         * \param exec_str Command to execute
         */
        void Exec(std::string& exec_str);

        /**
         * \brief Check existence of files and append to exec_str with spaces
         * \param files     Names of files to append
         * \param exec_str  The string to append to
         */
        void CheckAndAppend(std::vector<std::string>& files,
                            std::string& exec_str);

        /**
         * \brief Get path of prog_name or return "" if not in PATH
         * \param prog_name Name of executable program to find in PATH
         * \return Absolute system path of the given program
         */
        std::string GetPath(std::string& prog_name);
};

#endif
