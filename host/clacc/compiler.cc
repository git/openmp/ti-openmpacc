/******************************************************************************
 * Copyright (c) 2019, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 * \file compiler.cc
 * \brief Host, Target, S2S Compiler abstraction for CLACC
 */

#include "compiler.h"

/*********************************************************************/
/* Constructor: Check if compiler in PATH and do initial setup       */
/*********************************************************************/
Compiler::Compiler(std::string name, CompilerType type, bool debug)
{
    name_ = name;
    type_ = type;
    debug_ = debug;
    ready_to_compile_ = false;
    exec_return_value_ = -1;
    /* Check existence of the compiler in path */
    std::string compiler_path = GetPath(name_);
    if (compiler_path.compare("") == 0 )
    {
        std::cerr << ">> ERROR: Compiler " << name_
                  << " not in PATH. Exiting.." << std::endl;
        exit(EXIT_FAILURE);
    }
    if(debug_)
    {
        std::cout << ">> DEBUG: Using " << compiler_path << " for "
                  << CompilerTypeString[type] << " compilation."
                  << std::endl;
    }
}

/*********************************************************************/
/* Get compiler ready for compilation if not already done so         */
/*********************************************************************/
void Compiler::Setup()
{
    if (!ready_to_compile_)
    {
        cc_str_ = name_;
        cc_str_.append(" ");
        if (compile_opts_.size() == 0)
        {
            if (debug_)
                std::cout << ">> WARNING: No compile flags set for "
                          << name_
                          <<  std::endl;
        }else
        {
            cc_str_.append(GetStringOfElements(compile_opts_," "));
        }
        ready_to_compile_ = true;
    }
}

/*********************************************************************/
/* Check existence of files in vector and append to exec_str         */
/*********************************************************************/
void Compiler::CheckAndAppend(std::vector<std::string>& files,
                                          std::string& exec_str)
{
    for(auto file : files)
    {
        if (fs::exists(fs::path(file)))
        {
            exec_str.append(file);
            exec_str.append(" ");
        }else
        {
            std::cerr << ">> ERROR: Compiler.Compile: File "
                      << file << "does not exist." << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

/*********************************************************************/
/* Compile a single file to produce object file                      */
/*********************************************************************/
void Compiler::Compile(std::string& file)
{
    std::vector<std::string> files = {file};
    Compile(files);
}

/*********************************************************************/
/* Compile list of files to produce object files                     */
/*********************************************************************/
void Compiler::Compile(std::vector<std::string>& files)
{
    std::string exec_str = cc_str_;
    if (type_ == HOST)
        exec_str.append("-c ");

    /* Add file names to exec string */
    CheckAndAppend(files, exec_str);

    /* Compile */
    Exec(exec_str);
}

/*********************************************************************/
/* Compile and link single file to produce a.out                     */
/*********************************************************************/
void Compiler::Link(std::string& file, bool link_libs)
{
    std::vector<std::string> files = {file};
    Link(files,link_libs);
}


/*********************************************************************/
/* Compile and link list of files to produce a.out                   */
/*********************************************************************/
void Compiler::Link(std::vector<std::string>& files, bool link_libs)
{
    std::string exec_str = cc_str_;

    /* Add file names to exec string */
    CheckAndAppend(files, exec_str);

    if (link_lib_dependencies_.size() != 0 && link_libs)
        exec_str.append(GetStringOfElements(link_lib_dependencies_," "));

    /* Link and make a.out */
    Exec(exec_str);
}

/*********************************************************************/
/* Compile and link a single file into exe_name executable           */
/*********************************************************************/
void Compiler::Link(std::string& file, std::string& exe_name,
                    bool link_libs)
{
    std::vector<std::string> files = {file};
    Link(files,exe_name,link_libs);
}


/*********************************************************************/
/* Compile and link list of given files and add lib dependencies if  */
/* required to create exe_name executable                            */
/*********************************************************************/
void Compiler::Link(std::vector<std::string>& files,
                                std::string& exe_name,
                                bool link_libs)
{
    std::string exec_str = cc_str_;

    /* Add file names to exec string */
    CheckAndAppend(files, exec_str);

    if (link_lib_dependencies_.size() != 0 && link_libs)
        exec_str.append(GetStringOfElements(link_lib_dependencies_," "));
    exec_str.append("-o ");
    exec_str.append(exe_name);

    /* Link and make exe_name executable */
    Exec(exec_str);
}

/*********************************************************************/
/* Call system() on the given command exec string                    */
/*********************************************************************/
void Compiler::Exec(std::string& exec_str)
{
    try
    {
        exec_return_value_ = Syscall(exec_str, debug_);
    }
    catch(std::exception& e)
    {
        std::cout << e.what() << std::endl;
    }
}

/*********************************************************************/
/* Get the path of the given program name or return an empty string  */
/*********************************************************************/
std::string Compiler::GetPath(std::string& prog_name)
{
    char* path = getenv("PATH");
    if (!path)
        return "";

    std::string env_path(path);
    std::vector<std::string> paths_to_search;
    boost::split(paths_to_search, env_path, boost::is_any_of(":"));

    /* Iterate over search paths to check if prog_name exists */
    for (auto path : paths_to_search)
    {
        fs::path p(path);
        try
        {
            if (fs::exists(p))
            {
                if(fs::is_directory(p))
                {
                    std::vector<fs::path> dir_contents;
                    std::copy(fs::directory_iterator(p),
                              fs::directory_iterator(),
                              std::back_inserter(dir_contents)
                              );

                    for (auto file_path  : dir_contents)
                    {
                        fs::path file_name = file_path.filename();
                        if (prog_name.compare(file_name.string()) == 0)
                            return file_path.string();
                    }
                }
            }
        }catch(fs::filesystem_error& e)
        {
            std::cerr << ">> ERROR: Compiler.GetPath: " << e.what()
                      << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    return "";
}
