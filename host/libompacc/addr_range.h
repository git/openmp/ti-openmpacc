/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#pragma once

#include "oa_types.h"


namespace oa {

/// Represents a host address range as a (start, size) pair and provides 
/// the ==, < comparison operators
class AddressRange
{
    public:
        AddressRange(uintptr_t start, size_t size);

        bool      operator== (const AddressRange &ar) const;
        bool      operator<  (const AddressRange &ar) const;

        bool      contains   (const AddressRange &ar) const;
        size_t    size       ()                       const { return size_m; }
        uintptr_t start      ()                       const { return start_m;}

        string    to_string  ()                       const;

    private:
        uintptr_t end()   const { return start_m+size_m-1; }
        uintptr_t start_m;
        size_t    size_m;
};

inline AddressRange::AddressRange(uintptr_t start, size_t size):
                                            start_m(start),
                                            size_m(size)
{}

            
inline bool AddressRange::operator== (const AddressRange &ar) const
{
    return (start_m == ar.start_m && size_m == ar.size_m);
}


inline bool AddressRange::operator< (const AddressRange &ar) const
{
    return (start_m < ar.start_m);
}

/// Returns true if ar is fully contained in the address range
inline bool AddressRange::contains(const AddressRange &ar) const
{
    if (ar.start_m < start_m || ar.end() > end())
        return false;

    return true;
}

/// Dump function, only used for debug
inline std::string AddressRange::to_string() const
{
    stringstream ss;
    ss << '[' << std::hex << std::showbase << start_m << std::noshowbase 
       << std::dec << ':' << size_m << ']';

    return ss.str();
}


    
} // namespace oa
