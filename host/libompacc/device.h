/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#pragma once

#include "oa_types.h"
#include "oa_defs.h"
#include "data_region_stack.h"

#include <unordered_map>


namespace oa
{


class Region;
class DataMap;
class DeviceBuffer;
class TargetRegion;

/// Represents the interface that must be supported by any device
class DeviceInterface
{
    public:
        DeviceInterface(DeviceId id);
        virtual ~DeviceInterface();

        virtual void writeToDevice  (const DataMap *dm) =0;
        virtual void readFromDevice (const DataMap *dm) =0; 

        virtual uint64_t nameToDeviceAddress(const char *name) const = 0;

        virtual bool buildProgram(void) =0;

        virtual void prepareExecutionObject  (TargetRegion *er) =0;
        virtual void preExecuteDataMovement  (TargetRegion *er) =0;
        virtual void triggerExecution        (TargetRegion *er) =0;
        virtual void postExecuteDataMovement (TargetRegion *er) =0;

        virtual void waitForDataTransfer() = 0;

        virtual unique_ptr<DeviceBuffer> createBuffer (const DataMap &dm) =0;
        virtual unique_ptr<DeviceBuffer> reuseBuffer  (const DataMap &dm) =0;


        virtual void    pushRegion(Region *r) { region_stack_m.push(r); }
        virtual void    popRegion()           { region_stack_m.pop(); }
        virtual Region* peekRegion()          { return region_stack_m.peek(); }

        DataMap* findDataMap(const AddressRange &ar) const
        { return region_stack_m.findDataMap(ar); }

    private:
        RegionStack region_stack_m;
        DeviceId    id_m;
};

inline DeviceInterface::DeviceInterface(DeviceId id) : region_stack_m(id),
                                                       id_m(id) 
{}

inline DeviceInterface::~DeviceInterface() 
{} 




/// Collection of devices available on the system
/// Singleton 
class DeviceCollection
{
    public:
        ~DeviceCollection() {}
        static DeviceCollection& instance()
        {
            static DeviceCollection dc;
            return dc;
        }

        DeviceInterface* device(DeviceId id)
        { return devices_m[id].get(); }

    private:
        DeviceCollection(void) { init(); }
        DeviceCollection(const DeviceCollection& dc);
        DeviceCollection& operator= (const DeviceCollection& dc);

        void init();

        void addDevice(DeviceId id, unique_ptr<DeviceInterface> &device)
        {
            devices_m[id] = move(device);
        }

        // All devices available on the system
        // Use unique_ptr to delete DeviceInterface when DeviceCollection is
        // destroyed
        std::unordered_map<DeviceId, unique_ptr<DeviceInterface>> devices_m;
};


}
