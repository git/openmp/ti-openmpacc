#/******************************************************************************
 #* Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 #*   All rights reserved.
 #*
 #*   Redistribution and use in source and binary forms, with or without
 #*   modification, are permitted provided that the following conditions are met:
 #*       * Redistributions of source code must retain the above copyright
 #*         notice, this list of conditions and the following disclaimer.
 #*       * Redistributions in binary form must reproduce the above copyright
 #*         notice, this list of conditions and the following disclaimer in the
 #*         documentation and/or other materials provided with the distribution.
 #*       * Neither the name of Texas Instruments Incorporated nor the
 #*         names of its contributors may be used to endorse or promote products
 #*         derived from this software without specific prior written permission.
 #*
 #*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 #*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 #*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 #*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 #*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 #*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 #*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 #*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 #*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 #*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 #*   THE POSSIBILITY OF SUCH DAMAGE.
 #*****************************************************************************/
SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -O3 -std=gnu++11")
SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mfpu=neon -ftree-vectorize -march=armv7-a  -mfloat-abi=hard")

SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wcomment -Wformat -Wformat-security -Wreturn-type -Wtrigraphs -Winline -Wunused-function -Wunused-value -Wparentheses -Wmain -Werror -Wpointer-arith -Wcast-align -Wno-unused-parameter -Wno-sign-compare -Wno-switch -Wno-shadow -Wno-inline -Wno-unused -Wctor-dtor-privacy -Wuninitialized")

INCLUDE_DIRECTORIES (${CMAKE_SOURCE_DIR}/include)
INCLUDE_DIRECTORIES ($ENV{LINUX_DEVKIT_ROOT}/usr/include)
INCLUDE_DIRECTORIES ($ENV{TARGET_ROOTDIR}/usr/include)

FIND_LIBRARY (OPENCL_LIB OpenCL $ENV{TARGET_ROOTDIR}/usr/lib)
FIND_LIBRARY (OPENCL_UTIL_LIB ocl_util $ENV{TARGET_ROOTDIR}/usr/lib)

ADD_LIBRARY (OpenMPAcc SHARED ompacc.cc device.cc ocl_device.cc region.cc trace.cc data_region_stack.cc device_functions.cc)

SET(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib)
SET_TARGET_PROPERTIES(OpenMPAcc PROPERTIES
  VERSION ${${PROJECT_NAME}_VERSION}
  SOVERSION ${${PROJECT_NAME}_SOVERSION}
)
# List clacc dependencies
IF(CROSS_BUILD)
ADD_DEPENDENCIES(OpenMPAcc clacc_x86_exe)
ENDIF()
ADD_DEPENDENCIES(OpenMPAcc clacc_arm_exe)

TARGET_LINK_LIBRARIES (OpenMPAcc ${OPENCL_LIB})
TARGET_LINK_LIBRARIES (OpenMPAcc ${OPENCL_UTIL_LIB})

# Install OpenMPAcc library
INSTALL (TARGETS OpenMPAcc LIBRARY DESTINATION /usr/lib ${OMPACC_FPERMS})
