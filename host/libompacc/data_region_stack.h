/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#pragma once

#include "oa_types.h"
#include "addr_range.h"
#include "trace.h"

namespace oa {

class Region;
class DataMap;

/// Stack of data regions available on a given device
/// GOMP_target_data corresponds to a push, GOMP_target_data_end is a pop
class RegionStack
{
    public:
         RegionStack(DeviceId id): id_m(id) {}
        ~RegionStack() {}

        void    push (Region *r);
        void    pop  ();
        Region* peek () const;

        DataMap* findDataMap(const AddressRange &ar) const;

    private:
        list<Region*>  regions_m;
        DeviceId       id_m;
};

inline Region* RegionStack::peek() const
{
    // Calling peek on an empty list is undefined
    if (regions_m.empty()) return NULL;

    return regions_m.back();
}

inline void RegionStack::push(Region *r)
{
    TRACE::print("RS: pushing [%p]\n", r);
    regions_m.push_back(r);
}

} // namespace oa
