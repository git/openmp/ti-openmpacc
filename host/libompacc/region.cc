/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#include "region.h"
#include "data_map_dt.h"

using namespace oa;

TargetRegion::TargetRegion(DeviceId id, FnPtr fn_ptr, const char* fn_name):
                                            Region(id),
                                            host_fn_ptr(fn_ptr),
                                            host_fn_name(fn_name)
{
    device_m   = oa::DeviceCollection::instance().device(id);
}

void TargetRegion::initializeMaps(int mapnum,
                                    void **hostaddrs,
                                    unsigned int *sizes,
                                    unsigned char *kinds)
{
    for (int i = 0; i < mapnum; i++)
    {
        uintptr_t addr = reinterpret_cast<uintptr_t>(hostaddrs[i]);
        size_t    size = static_cast<size_t>(sizes[i]);

        MapKinds mk = static_cast<MapKinds>(kinds[i]);
        DataMap *dmp;
        if (mk == mk_MAP_PTR)
        {
            dmp = new DataMapPtr(addr, size, mc_TARGET);
        }
        else
            dmp = new DataMap(mk, addr, size, mc_TARGET);

        TRACE::print("TR:initMap %s\n", dmp->to_string().c_str());

        AddressRange ar(dmp->start(), dmp->size());


        //if (dm is not present in DataRegion and dm is not local)
        DataMap *mapped_dm = NULL;
        if (dmp->isLocal())
            ;
        else if ((mapped_dm = device_m->findDataMap(ar)) != NULL)
        {
            TRACE::print("TR: found %s for %s\n",
                                        mapped_dm->to_string().c_str(),
                                        ar.to_string().c_str());

            if (mapped_dm->kind() == mk_MAP_PTR && mk != mk_MAP_PTR)
            {
                ERROR(1,  "Variables accessed via a pointer "
                          "in a target region must be shaped using array "
                          "sections e.g. map(to:ptr[start:size]). This is "
                          "an implementation restriction in the current runtime"
                          " and will be fixed in a future release.");
            }

            dmp->reuseBuffer(device_m, mapped_dm);
        }
        else
            dmp->createBuffer(device_m);

        unique_ptr<DataMap> dm { dmp };
        data_maps_m.push_back(move(dm));
    }
}

void TargetRegion::syncDataToTarget()
{
    device_m->preExecuteDataMovement(this);
    TRACE::print("\tTargetRegion::Host -> Target\n");
    // Copy data to device memory
    for (auto const &dm_uptr: data_maps_m)
    {
        DataMap *dm = dm_uptr.get();

        if (dm->isLocal())
            continue;

        switch (dm->creator())
        {
            case mc_TARGET:
            {
                if (dm->isTo())
                    dm->writeToDevice();
                break;
            }
            case mc_TARGET_DATA:
                break;

            default:
                ERROR(1, "Invalid Map source kind");
        }
    }
}


void TargetRegion::syncDataFromTarget()
{
    // Copy data to host memory
    TRACE::print("\tTargetRegion::Target -> Host\n");
    for (auto const &dm_uptr: data_maps_m)
    {
        DataMap *dm = dm_uptr.get();
        if (dm->isLocal())
            continue;

        if (!dm->isFrom())
            continue;

        switch (dm->creator())
        {
            case mc_TARGET:
                {
                    dm->readFromDevice();
                    break;
                }
            case mc_TARGET_DATA:
                break;

            default:
                ERROR(1, "Invalid Map source kind");
        }
    }
    device_m->postExecuteDataMovement(this);
}


DataRegion::DataRegion(DeviceId id): Region(id)
{
    device_m   = oa::DeviceCollection::instance().device(id);
}

void DataRegion::initializeMaps(int mapnum,
                                    void **hostaddrs,
                                    unsigned int *sizes,
                                    unsigned char *kinds)
{
    for (int i = 0; i < mapnum; i++)
    {
        uintptr_t addr = reinterpret_cast<uintptr_t>(hostaddrs[i]);
        size_t    size = static_cast<size_t>(sizes[i]);

        AddressRange ar(addr, size == 0 ? sizeof(HostPtr) : size);

        // If the range is already mapped by an outer target data region,
        // no bookkeeping required.
        DataMap *mapped_dm = NULL;
        if ((mapped_dm = device_m->findDataMap(ar)) != NULL)
        {
            TRACE::print("DR: found in %s\n", mapped_dm->to_string().c_str());
            continue;
        }

        MapKinds mk = static_cast<MapKinds>(kinds[i]);
        DataMap *dm;
        if (mk == mk_MAP_PTR)
        {
            dm = new DataMapPtr(addr, size, mc_TARGET_DATA);
        }
        else
            dm = new DataMap(static_cast<MapKinds>(kinds[i]),
                              addr, size, mc_TARGET_DATA);

        TRACE::print("DR:initMap %s\n", dm->to_string().c_str());

        if (dm->isLocal())
            ERROR(1, "local not allowed in target data");

        dm->createBuffer(device_m);

        // Find the right spot and insert into list
        insert(dm);
    }

}

void DataRegion::addDeclTargetMaps(DeclVarMap const *table)
{
    DeclVarMap const* curr = table;
    while (curr->name != NULL)
    {
        DataMapDeclTarget *dm = new DataMapDeclTarget(id_m,
                                       reinterpret_cast<uintptr_t>(curr->addr),
                                       curr->size, curr->name);
        insert(dm);
        TRACE::print("DR:addDeclTargetMaps %s\n", dm->to_string().c_str());
        curr += 1;
    }
}


void DataRegion::updateMaps(int mapnum,
                                    void **hostaddrs,
                                    unsigned int *sizes,
                                    unsigned char *kinds)
{
    for (int i = 0; i < mapnum; i++)
    {
        uintptr_t addr = reinterpret_cast<uintptr_t>(hostaddrs[i]);
        size_t    size = static_cast<size_t>(sizes[i]);
        MapKinds  mk   = static_cast<MapKinds>(kinds[i]);

        AddressRange ar(addr, size == 0 ? sizeof(HostPtr) : size);

        DataMap *mapped_dm = NULL;
        if ((mapped_dm = device_m->findDataMap(ar)) == NULL)
        {
            TRACE::print("DR: %s not found\n", ar.to_string().c_str());
            continue;
        }

        bool isDataMapDeclTarget =
                  (dynamic_cast<DataMapDeclTarget *>(mapped_dm) != NULL);

        DataMap *dm;
        if (isDataMapDeclTarget)
        {
            dm = mapped_dm;

            // Override the map kind with the one specified by update
            dm->setKind(mk);
        }
        else
        {

            if (mk == mk_MAP_PTR)
            {
                dm = new DataMapPtr(addr, size, mc_TARGET_DATA);
            }
            else
                dm = new DataMap(static_cast<MapKinds>(kinds[i]),
                                  addr, size, mc_TARGET_DATA);


            if (dm->isLocal())
                ERROR(1, "local not allowed in target data");

            dm->reuseBuffer(device_m, mapped_dm);
        }

        TRACE::print("DR:updateMap %s\n", dm->to_string().c_str());

        if (dm->isTo())
            dm->writeToDevice();
        else if (dm->isFrom())
            dm->readFromDevice();

        if (!isDataMapDeclTarget)
            delete dm;
    }

}

void DataRegion::syncDataToTarget()
{
    for (auto const dm: data_maps_m)
    {
        if (dm->isTo())
            dm->writeToDevice();
    }

}

void DataRegion::syncDataFromTarget()
{
    for (auto const dm: data_maps_m)
    {
        if (dm->isFrom())
            dm->readFromDevice();
    }

    // Ensure data transfers to host complete before host accesses the data
    device_m->waitForDataTransfer();
}

DataRegion::~DataRegion()
{
    for (DataMap* dm: data_maps_m)
        delete dm;
}


DataMap* DataRegion::isMapped(const AddressRange &ar) const
{
    for (auto const dm: data_maps_m)
    {
        //TRACE::print("isMapped: %s\n", dm->to_string().c_str());
        if (dm->contains(ar))
            return dm;
    }

    return NULL;
}

void DataRegion::insert(DataMap *dm)
{
    if (data_maps_m.empty())
    {
        data_maps_m.push_front(dm);
        return;
    }

    auto it = data_maps_m.begin();
    while (it != data_maps_m.end() && (**it < *dm))
    {
        //TRACE::print("Insert %s\n", (*it)->to_string().c_str());
        it++;
    }

    if (it == data_maps_m.end())
        data_maps_m.push_back(dm);
    else
        data_maps_m.insert(it, dm);
}

