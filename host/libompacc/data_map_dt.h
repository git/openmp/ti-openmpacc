/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#pragma once

#include "data_map.h"

namespace oa
{

/// Represents mapping a single variable onto the device
/// Delegates to the DeviceInterface for allocating storage and data movement
class DataMapDeclTarget: public DataMap
{
    public:
        DataMapDeclTarget(DeviceId id, uintptr_t hostPtr, 
                          size_t size, const char* name);
        virtual ~DataMapDeclTarget();

        // DeviceBuffer related methods
        virtual void createBuffer (DeviceInterface *d) {}
        virtual void reuseBuffer  (DeviceInterface *d, 
                                   const DataMapDeclTarget* dm) {}

        virtual void writeToDevice();
        virtual void readFromDevice();

        virtual DeviceBuffer* buffer() const { ASSERT_FAIL; return NULL; }

        uint64_t deviceAddress() const { return device_addr_m; }


    private:

        void updateDeviceAddress(void);

        uint64_t         device_addr_m;
        const char*      sym_name_m;
        DeviceInterface *device_m;
};

inline DataMapDeclTarget::DataMapDeclTarget(DeviceId    id,
                                            uintptr_t   start, 
                                            size_t      size,
                                            const char* name)
                                        : DataMap(mk_TO_FROM, start, size,
                                                  mc_DECL_TARGET),
                                          device_addr_m(0),
                                          sym_name_m(name)
{
    device_m = DeviceCollection::instance().device(id);
    assert(device_m != 0);
}



inline void DataMapDeclTarget::updateDeviceAddress()
{
    if (device_addr_m == 0)
    {
        device_addr_m = device_m->nameToDeviceAddress(sym_name_m);
        assert(device_addr_m != 0);
    }
}

inline void DataMapDeclTarget::writeToDevice()  
{ 
    updateDeviceAddress();
    device_m->writeToDevice(this);
}

inline void DataMapDeclTarget::readFromDevice() 
{ 
    updateDeviceAddress();
    device_m->readFromDevice(this);
}

inline DataMapDeclTarget::~DataMapDeclTarget() {}



} // namespace oa
