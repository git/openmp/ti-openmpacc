/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#pragma once

#include "buffer.h"
#include "oa_cl.h"

namespace oa {

/// Implements DeviceBuffer for OpenCL devices    
class OclBuffer: public DeviceBuffer
{
    public:
        OclBuffer(DeviceInterface *d, cl_mem b, bool in_cmem=false);
        OclBuffer(const OclBuffer &b);
        virtual ~OclBuffer();

        virtual void writeToDevice (const DataMap* dm);
        virtual void readFromDevice(const DataMap* dm);

        bool   hostPtrInCMEM() const { return hptr_in_cmem_m; }
        cl_mem clBuffer()            { return buffer_m; }

    private:
        DeviceInterface *device_m;
        cl_mem           buffer_m;
        cl_event         event_m;
        bool             hptr_in_cmem_m;
};

inline OclBuffer::OclBuffer(DeviceInterface *d, cl_mem b, bool in_cmem): 
                                                    device_m(d),
                                                    buffer_m(b),
                                                    hptr_in_cmem_m(in_cmem)
{
    TRACE::print("\tOCL Create B:%p%s\n", buffer_m, 
                                          hptr_in_cmem_m? " CMEM":"");
}

inline OclBuffer::OclBuffer(const OclBuffer &b):
                                            device_m(b.device_m),
                                            buffer_m(b.buffer_m),
                                            hptr_in_cmem_m(b.hptr_in_cmem_m)
{
    TRACE::print("\tOCL Retain B:%p\n", buffer_m);
    cl_int ret = clRetainMemObject(buffer_m);
    assert (ret == CL_SUCCESS);
}

inline OclBuffer::~OclBuffer()
{
    TRACE::print("\tOCL Release B:%p\n", buffer_m);
    cl_int ret = clReleaseMemObject(buffer_m);
    assert (ret == CL_SUCCESS);
}

inline void OclBuffer::writeToDevice(const DataMap *dm)
{
    device_m->writeToDevice(dm);
}

inline void OclBuffer::readFromDevice(const DataMap *dm)
{
    device_m->readFromDevice(dm);
}


} // namespace oa
