/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#pragma once

#include "ti_omp_device.h"

typedef unsigned int DeviceId;
typedef void*        HostPtr;

typedef unsigned long long DSPDevicePtr64;

typedef void (*FnPtr) (void *);

#include <memory>
using std::unique_ptr;

#include <cstddef>
using std::size_t;

#include <cstdint>
using std::uintptr_t;

#include <sstream>
using std::stringstream;

#include <string>
using std::string;

#include <list>
using std::list;

#include <vector>
using std::vector;

enum MapKinds
{
    mk_ALLOC       = 0x0,
    mk_TO,
    mk_FROM,
    mk_TO_FROM,
    mk_MAP_PTR,
    mk_TO_L2,
    mk_LAST
};

const char * const MapKindNames[(int)mk_LAST+1] = 
{    
    "alloc",
    "H->T",
    "H<-T",
    "H<->T",
    "mapptr",
    "tol2",
    "last"
};

enum MapCreator
{    
    mc_UNSPECIFIED             = 0,
    mc_TARGET,
    mc_TARGET_DATA,
    mc_DECL_TARGET,
    mc_LAST
};

const char * const MapCreatorNames[(int)mc_LAST + 1] =
{
    "Unspecified",
    "tgt",
    "tgt data",
    "decl tgt",
    "last"
};


typedef struct 
{
    void*        addr;
    unsigned int size;
    const char*  name;
} DeclVarMap;

