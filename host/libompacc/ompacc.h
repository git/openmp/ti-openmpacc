/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 * \file ompacc.h
 * \brief OpenMP 4.0 Accelerator Model API 
 */

#ifndef OMPACC_H_
#define OMPACC_H_

//#include "accelerator.h"

extern "C" {
/**
 * \brief Create a target region on the given device and execute the 
 *        given function on it
 * \param device    Device ID 
 * \param fn        Function pointer to target kernel
 * \param fnname    Name of target kernel function
 * \param mapnum    Number of kernel arguments being mapped i.e. 
 *                  number of fields in structs used here
 * \param hostaddrs Pointer to address of struct containing 
 *                  pointers to kernel argument variables
 * \param sizes     Pointer to struct containing sizes of argument 
 *                  variable data
 * \param kinds     Pointer to struct containing types of argument 
 *                  variable data (1 = to, 2 = from, 3 = tofrom) 
 */

    void GOMP_target (  int device,            
                        void (*fn) (void *),                          
                        char *fnname,         
                        unsigned int mapnum,   
                        void **hostaddrs,     
                        unsigned int *sizes,   
                        unsigned char *kinds   
                        );
    
/**
 * \brief Create a data region on the given target device 
 * \param device    Device ID 
 * \param mapnum    Number of data sections being mapped i.e. 
 *                  number of fields in structs used here
 * \param hostaddrs Pointer to address of struct containing 
 *                  pointers to data section variables
 * \param sizes     Pointer to struct containing sizes of data sections
 * \param kinds     Pointer to struct containing types of argument 
 *                  variable data (1 = to, 2 = from, 3 = tofrom) 
 */
    void GOMP_target_data (  int device,            
                             unsigned int mapnum,   
                             void **hostaddrs,     
                             unsigned int *sizes,   
                             unsigned char *kinds   
                             );
 /**
 * \brief Destroy a data region on the given target device 
 * \param device    Device ID 
 * \param mapnum    Number of data sections being mapped i.e. 
 *                  number of fields in structs used here
 * \param hostaddrs Pointer to address of struct containing 
 *                  pointers to data section variables
 * \param sizes     Pointer to struct containing sizes of data sections
 * \param kinds     Pointer to struct containing types of argument 
 *                  variable data (1 = to, 2 = from, 3 = tofrom) 
 */
    void GOMP_target_data_end (  int device,            
                             unsigned int mapnum,   
                             void **hostaddrs,     
                             unsigned int *sizes,   
                             unsigned char *kinds   
                             );
 
/**
 * \brief Update data sections to or from a data region
 * \param device    Device ID 
 * \param mapnum    Number of data sections being mapped i.e. 
 *                  number of fields in structs used here
 * \param hostaddrs Pointer to address of struct containing 
 *                  pointers to data section variables
 * \param sizes     Pointer to struct containing sizes of data sections
 * \param kinds     Pointer to struct containing types of argument 
 *                  variable data (1 = to, 2 = from, 3 = tofrom) 
 */
    void GOMP_target_update (  int device,            
                               unsigned int mapnum,   
                               void **hostaddrs,     
                               unsigned int *sizes,   
                               unsigned char *kinds   
                               );

void omp_set_default_device (int device_num);
int omp_get_default_device (void);
int omp_get_num_devices (void);
int omp_is_initial_device (void);

}
#endif /* OMPACC_H_ */
