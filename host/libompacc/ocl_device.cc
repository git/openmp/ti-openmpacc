/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#include <cstdlib>
using std::size_t;
#include <ti/cmem.h>

#include "ocl_device.h"
#include "ocl_buffer.h"
#include "region.h"
#include "data_map_dt.h"

extern "C" int32_t __device_write(int32_t dsp, DSPDevicePtr64 addr,
                                  uint8_t *buf, uint32_t sz);

extern "C" int32_t __device_read (int32_t dsp, DSPDevicePtr64 addr,
                                  uint8_t *buf, uint32_t sz);
extern "C" uint64_t __query_symbol(cl_program program, const char *sym_name);


using namespace oa;

/// Initialize an OclDevice
OclDevice::OclDevice(DeviceId id, cl_device_type type) : DeviceInterface (id)
{
    TRACE::print("\tOCL Device %d created\n", id);

    cl_int errcode;
    context_m = clCreateContextFromType(0,          // properties
                                        type,       // device_type
                                        0,          // pfn_notify
                                        0,          // user_data
                                        &errcode);

    errcode = clGetDeviceIDs(0,         // platform
                             type,      // device_type
                             1,         // num_entries
                             &device_m, // devices
                             0);        // num_devices

    queue_m    = clCreateCommandQueue(context_m,
                                      device_m,
                                      0,            // properties
                                      &errcode);
    errorCheck(errcode);

    buildProgram();
    performed_cmemWbInvAll_m = false;
}


/// Create an OpenCL buffer for the DataMap
/// The created buffer is wrapped in a unique_ptr to ensure it's
/// dtor is invoked when the corresponding DataMap is destroyed
unique_ptr<DeviceBuffer> OclDevice::createBuffer(const DataMap &dm)
{
    size_t       size = dm.size();
    cl_mem_flags flag = mapKind2memFlag(dm.kind());

    void *host_ptr = reinterpret_cast<void *>(dm.start());

    bool hostPtrInCMEM = __is_in_malloced_region(host_ptr);

    if (hostPtrInCMEM)
    {
        flag |= (cl_mem_flags)(CL_MEM_USE_HOST_PTR | CL_MEM_HOST_NO_ACCESS);

        // If a variable allocated via __malloc* function is annotated from,
        // the runtime performs a CMEM_cacheInv operation on the associated
        // range after the target region completes execution. This operation
        // does not work as expected. The workaround is to switch FROM to
        // TO_FROM resulting in a CMEM_cacheWbInv before the region
        if (dm.kind() == mk_FROM)
            dm.setKind(mk_TO_FROM);
    }

    cl_int       errcode;
    cl_mem buffer = clCreateBuffer(context_m,
                                   flag,
                                   size,
                                   hostPtrInCMEM ? host_ptr : NULL,
                                   &errcode);
    errorCheck(errcode);

    return unique_ptr<DeviceBuffer> {new OclBuffer(this, buffer, hostPtrInCMEM)                            };
}


unique_ptr<DeviceBuffer> OclDevice::reuseBuffer(const DataMap &dm)
{
    OclBuffer *ocl = dynamic_cast<OclBuffer *>(dm.buffer());
    return unique_ptr<DeviceBuffer> {new OclBuffer(*ocl) };
}


extern unsigned int  __attribute__((weak)) __TI_CLACC_KERNEL_dsp_bin_len;
extern unsigned char __attribute__((weak)) __TI_CLACC_KERNEL_dsp_bin[];

bool OclDevice::buildProgram(void)
{
    if (&__TI_CLACC_KERNEL_dsp_bin_len == NULL ||
        &__TI_CLACC_KERNEL_dsp_bin == NULL)
        return false;

    const unsigned char *bin_arr = __TI_CLACC_KERNEL_dsp_bin;
    const size_t         bin_len = __TI_CLACC_KERNEL_dsp_bin_len;

    cl_int err;
    program_m = clCreateProgramWithBinary(context_m,
                                          1,            // num_devices
                                          &device_m,    // device_list
                                          &bin_len,
                                          &bin_arr,
                                          0,            // binary_status
                                          &err);
    errorCheck(err);

    const char *compile_options = "";
    err = clBuildProgram(program_m, 1, &device_m, compile_options, 0, 0);
    errorCheck(err);

    return true;
}

/// Release resources associated with an OpenCL device
OclDevice::~OclDevice()
{
    clReleaseProgram      (program_m);
    clReleaseCommandQueue (queue_m);
    clReleaseContext      (context_m);
}


/// Create an OpenCL kernel and initialize it's arguments
void OclDevice::prepareExecutionObject  (TargetRegion *tr)
{
    TRACE::print("\tOclDevice::Initialize kernel\n");
    // Create kernel and set arguments
    std::string fname(tr->funcName());
    fname.append("_wrapper");

    cl_int err;
    kernel_m = clCreateKernel(program_m, fname.c_str(), &err);

    // The OCL kernel wrapper expects the following sequence
    // of arguments for a shaped array section:
    // 1) OCL Device Buffer for shaped array section
    // 2) MAP_PTR (4 byte OCL device buffer)
    // 3) Original bias to set the value of (2) MAP_PTR such that
    //    it points to the start of (1) OCL device buffer
    int arg = 0;
    for (auto const &dm_uptr: *tr)
    {
        const DataMap *dm = dm_uptr.get();
        if (dm->isLocal())
        {
            clSetKernelArg(kernel_m, arg++, dm->size(), NULL);
            TRACE::print("  Arg[%d]: local, %d\n", arg-1, dm->size());
            continue;
        }

        OclBuffer *obuf   = dynamic_cast<OclBuffer *>(dm->buffer());
        cl_mem     buffer = obuf->clBuffer();
        clSetKernelArg(kernel_m, arg++, sizeof(cl_mem), &buffer);
        TRACE::print("  Arg[%d]: %p\n", arg-1, buffer);

        // If MapPtr, add bias w.r.t original buffer as argument
        if (dynamic_cast<const DataMapPtr *>(dm))
        {
            clSetKernelArg(kernel_m, arg++, sizeof(*dm->biasPtr()),
                           dm->biasPtr());
            TRACE::print("  Arg[%d]: bias %d\n", arg-1, *dm->biasPtr());
        }
    }
}

void OclDevice::preExecuteDataMovement  (TargetRegion *tr)
{
    // For shared buffers in CMEM, determine if it's profitable to perform
    // wbInvAll across all the buffers vs. wbInv for each buffer
    #if (CMEM_VERSION > 0x04000000)

    const size_t CMEM_WBINVALL_THRESHOLD = (8 << 20); // 8MB
    const int    NUM_BUFFER_THRESHOLD    = 8;

    size_t   totalSize = 0;
    uint32_t cmemBufCount = 0;
    for (auto const &dm_uptr: *tr)
    {
        const DataMap *dm = dm_uptr.get();
        if (dm->isLocal())
            continue;
        OclBuffer* clBuf    = dynamic_cast<OclBuffer *>(dm->buffer());
        if (clBuf && clBuf->hostPtrInCMEM())
        {
            totalSize += dm->size();
            cmemBufCount++;
        }
    }

    // Compute profitability:
    // 1. Total size of buffers in CMEM > 8MB
    // 2. >=8 individual buffers and total size >= 4MB. Factors in
    //    cost of multiple invocations of CMEM_cacheWbInv range function
    if (totalSize >= CMEM_WBINVALL_THRESHOLD ||
        (cmemBufCount >= NUM_BUFFER_THRESHOLD
            && (totalSize >= CMEM_WBINVALL_THRESHOLD/2)))
    {
        TRACE::print("\tOclDevice::CMEM wbInvAll [%d MB, %d]\n",
                      totalSize >> 20, cmemBufCount);
        CMEM_cacheWbInvAll();
        performed_cmemWbInvAll_m = true;
    }

    #endif

}

/// Enqueue the OpenCL kernel for execution
void OclDevice::triggerExecution        (TargetRegion *tr)
{
    // Execute kernel
    TRACE::print("\tOclDevice::Execute kernel\n");
    cl_int ret = clEnqueueTask(queue_m, kernel_m, 0, 0, &k_event_m);
    errorCheck(ret);
    TRACE::print("\tOclDevice::Execute kernel waiting...\n");
    ret = clWaitForEvents(1, &k_event_m);
    errorCheck(ret);
    TRACE::print("\tOclDevice::Release kernel events\n");
    ret = clReleaseEvent(k_event_m);
    errorCheck(ret);
    TRACE::print("\tOclDevice::Execute kernel complete\n");
}


void OclDevice::postExecuteDataMovement (TargetRegion *tr)
{
    performed_cmemWbInvAll_m = false;

    // Wait for any memory transfers to complete
    clFinish(queue_m);
    clReleaseKernel(kernel_m);
    kernel_m = NULL;
}

void OclDevice::waitForDataTransfer()
{
    clFinish(queue_m);
}

void OclDevice::writeToDevice(const DataMap *dm)
{
    void*      host_ptr = reinterpret_cast<void *>(dm->start());
    size_t     size     = dm->size();

    // If the data map is for a declare target variable, the storage for the
    // variable on the device is associated with the device binary.
    const DataMapDeclTarget *dmdt =
                                dynamic_cast<const DataMapDeclTarget *>(dm);
    if (dmdt)
    {
        __device_write(0, dmdt->deviceAddress(),
                       static_cast<uint8_t *>(host_ptr), size);
        return;
    }


    OclBuffer* clBuf    = dynamic_cast<OclBuffer *>(dm->buffer());

    if (clBuf->hostPtrInCMEM())
    {
        if (!performed_cmemWbInvAll_m)
        {
            TRACE::print("\tOCL: wbInv on %s\n", dm->to_string().c_str());
            if      (dm->kind() == mk_TO_FROM) CMEM_cacheWbInv(host_ptr, size);
            else if (dm->kind() == mk_TO)      CMEM_cacheWb(host_ptr, size);
        }
        return;
    }


    cl_mem clbuf = clBuf->clBuffer();

    cl_int ret = clEnqueueWriteBuffer(queue_m,
                                         clbuf,
                                         CL_FALSE,
                                         0,
                                         size,
                                         host_ptr,
                                         0,
                                         NULL,
                                         NULL);
    assert (ret == CL_SUCCESS);
}

void OclDevice::readFromDevice(const DataMap *dm)
{
    void*      host_ptr = reinterpret_cast<void *>(dm->start());
    size_t     size     = dm->size();

    const DataMapDeclTarget *dmdt =
                                dynamic_cast<const DataMapDeclTarget *>(dm);
    if (dmdt)
    {
        __device_read(0, dmdt->deviceAddress(),
                       static_cast<uint8_t *>(host_ptr), size);
        //CMEM_cacheInv(host_ptr, size);
        return;
    }

    OclBuffer* clBuf    = dynamic_cast<OclBuffer *>(dm->buffer());

    if (clBuf->hostPtrInCMEM())
    {
        // Need to invalidate iff buffer was not wbInv earlier due to a map to
        if (!dm->isTo() && !performed_cmemWbInvAll_m)
        {
            TRACE::print("\tOCL: Inv on %s\n", dm->to_string().c_str());
            CMEM_cacheInv(host_ptr, size);
        }
        return;
    }

    cl_mem clbuf = clBuf->clBuffer();

    cl_int ret = clEnqueueReadBuffer(queue_m,
                                        clbuf,
                                        CL_FALSE,
                                        0,
                                        size,
                                        host_ptr,
                                        0,
                                        NULL,
                                        NULL);
    assert (ret == CL_SUCCESS);
}


/// Map OMPA map flags to OCL buffer flags
cl_mem_flags OclDevice::mapKind2memFlag(MapKinds k)
{
    cl_mem_flags mem_flags = CL_MEM_READ_ONLY;

    switch (k)
    {
        case mk_TO:
            mem_flags = CL_MEM_READ_ONLY;
            break;
        case mk_FROM:
            mem_flags = CL_MEM_WRITE_ONLY;
            break;
        case mk_TO_FROM:
            mem_flags = CL_MEM_READ_WRITE;
            break;
        case mk_ALLOC:
        case mk_MAP_PTR:
            mem_flags = CL_MEM_READ_WRITE;
            break;
        default:
            ERROR(1, "Invalid MapKind");
    }
    return mem_flags;
}


uint64_t OclDevice::nameToDeviceAddress(const char *name) const
{
    return __query_symbol(program_m, name);
}


/// Convert OpenCL error codes to a string
const char * OclDevice::error2string(cl_int err)
{
    switch(err)
    {
         case   0: return "CL_SUCCESS";
         case  -1: return "CL_DEVICE_NOT_FOUND";
         case  -2: return "CL_DEVICE_NOT_AVAILABLE";
         case  -3: return "CL_COMPILER_NOT_AVAILABLE";
         case  -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
         case  -5: return "CL_OUT_OF_RESOURCES";
         case  -6: return "CL_OUT_OF_HOST_MEMORY";
         case  -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
         case  -8: return "CL_MEM_COPY_OVERLAP";
         case  -9: return "CL_IMAGE_FORMAT_MISMATCH";
         case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
         case -11: return "CL_BUILD_PROGRAM_FAILURE";
         case -12: return "CL_MAP_FAILURE";

         case -30: return "CL_INVALID_VALUE";
         case -31: return "CL_INVALID_DEVICE_TYPE";
         case -32: return "CL_INVALID_PLATFORM";
         case -33: return "CL_INVALID_DEVICE";
         case -34: return "CL_INVALID_CONTEXT";
         case -35: return "CL_INVALID_QUEUE_PROPERTIES";
         case -36: return "CL_INVALID_COMMAND_QUEUE";
         case -37: return "CL_INVALID_HOST_PTR";
         case -38: return "CL_INVALID_MEM_OBJECT";
         case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
         case -40: return "CL_INVALID_IMAGE_SIZE";
         case -41: return "CL_INVALID_SAMPLER";
         case -42: return "CL_INVALID_BINARY";
         case -43: return "CL_INVALID_BUILD_OPTIONS";
         case -44: return "CL_INVALID_PROGRAM";
         case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
         case -46: return "CL_INVALID_KERNEL_NAME";
         case -47: return "CL_INVALID_KERNEL_DEFINITION";
         case -48: return "CL_INVALID_KERNEL";
         case -49: return "CL_INVALID_ARG_INDEX";
         case -50: return "CL_INVALID_ARG_VALUE";
         case -51: return "CL_INVALID_ARG_SIZE";
         case -52: return "CL_INVALID_KERNEL_ARGS";
         case -53: return "CL_INVALID_WORK_DIMENSION";
         case -54: return "CL_INVALID_WORK_GROUP_SIZE";
         case -55: return "CL_INVALID_WORK_ITEM_SIZE";
         case -56: return "CL_INVALID_GLOBAL_OFFSET";
         case -57: return "CL_INVALID_EVENT_WAIT_LIST";
         case -58: return "CL_INVALID_EVENT";
         case -59: return "CL_INVALID_OPERATION";
         case -60: return "CL_INVALID_GL_OBJECT";
         case -61: return "CL_INVALID_BUFFER_SIZE";
         case -62: return "CL_INVALID_MIP_LEVEL";
         case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
         default: return "Unknown OpenCL error";
    }
}

