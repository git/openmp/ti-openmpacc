/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#pragma once


#include "oa_types.h"
#include "data_map.h"

namespace oa {

/// Abstract class representing a region (target data or target)    
class Region
{
    public:
        Region(DeviceId id) : id_m(id) {}
        virtual ~Region() {}

        virtual DeviceId id() const { return id_m; }

        virtual void syncDataFromTarget() =0;
        virtual void syncDataToTarget  () =0;

        virtual DataMap* isMapped(const AddressRange &ar) const  =0;

    protected:
        DeviceId    id_m;
};


/// Implements Region for 'target data' regions
class DataRegion : public Region
{
    public:
        DataRegion(DeviceId device_id);
        virtual ~DataRegion();

        void initializeMaps(int nummaps, void **hostaddrs, unsigned int *sizes,
                            unsigned char *kinds);

        // Perform any sync operations on buffers that have already been mapped
        void updateMaps(int nummaps, void **hostaddrs, unsigned int *sizes,
                        unsigned char *kinds);

        void addDeclTargetMaps(DeclVarMap const *table);

        virtual DataMap* isMapped(const AddressRange &ar) const;

        virtual void syncDataFromTarget();
        virtual void syncDataToTarget  ();

    private:
        void insert(DataMap *dm);

        DeviceInterface* device_m;      // device associated with region
        list<DataMap *>  data_maps_m;   // All maps associated with region
};


/// Implements Region for 'target' regions
class TargetRegion: public Region
{
    public:
        TargetRegion(DeviceId id, FnPtr fn_ptr, const char *fn_name);
        virtual ~TargetRegion() {}

        void initializeMaps(int mapnum, void **hostaddrs, unsigned int *sizes,
                            unsigned char *kinds);

        void prepareExecutionObject();
        void syncDataToTarget();
        void syncDataFromTarget();
        void triggerExecution();

        const std::string& funcName() const { return host_fn_name; }
        
        virtual DataMap* isMapped(const AddressRange &ar) const 
        { ASSERT_FAIL; return NULL; }

        typedef vector<unique_ptr<DataMap>>::iterator       iterator;
        typedef vector<unique_ptr<DataMap>>::const_iterator const_iterator;

        iterator       begin ()       { return data_maps_m.begin();  }
        iterator       end   ()       { return data_maps_m.end  ();  }
        const_iterator cbegin() const { return data_maps_m.cbegin(); }
        const_iterator cend  () const { return data_maps_m.cend  (); }

    private:
        DeviceInterface*             device_m;
        vector<unique_ptr<DataMap>>  data_maps_m;
        FnPtr                        host_fn_ptr;
        std::string                  host_fn_name;
};

inline void TargetRegion::prepareExecutionObject()
{
    device_m->prepareExecutionObject(this);
}

inline void TargetRegion::triggerExecution()
{
    device_m->triggerExecution(this);
}


} // namespace oa

