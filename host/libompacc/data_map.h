/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#pragma once

#include "addr_range.h"
#include "buffer.h"
#include "device.h"
#include "oa_defs.h"

namespace oa
{

/// Represents mapping a single variable onto the device
/// Delegates to the DeviceInterface for allocating storage and data movement
class DataMap
{
    public:
        DataMap(MapKinds m, uintptr_t start, size_t size, MapCreator c);
        virtual ~DataMap();

        // Comparison functions
        virtual bool operator== (const DataMap &dm) const;
        virtual bool operator<  (const DataMap &dm) const;
        virtual bool contains   (const DataMap &dm) const;
        virtual bool contains   (const AddressRange &ar) const;

        // Query functions
        virtual uintptr_t     start()   const;
        virtual size_t        size()    const;
        virtual MapKinds      kind()    const;
        virtual bool          isLocal() const;
        virtual MapCreator    creator() const;
        virtual size_t        bias()    const;
        virtual const size_t* biasPtr() const;
        virtual bool          isTo()    const;
        virtual bool          isFrom()  const;

        // DeviceBuffer related methods
        virtual void createBuffer (DeviceInterface *d);
        virtual void reuseBuffer  (DeviceInterface *d, const DataMap* dm);

        virtual void writeToDevice()  { buffer_m->writeToDevice(this); }
        virtual void readFromDevice() { buffer_m->readFromDevice(this); }

        virtual DeviceBuffer* buffer() const { return buffer_m.get(); }

        // Convert to string for debug messages
        virtual std::string to_string() const;

        /// map_kind_m is annotated mutable to allow it to be modified
        /// by setKind. This is to enable a workaround for CMEM
        virtual void setKind(MapKinds k) const { map_kind_m = k; }
        
    private:
        AddressRange             host_range_m;
        mutable MapKinds         map_kind_m;
        MapCreator               map_creator_m;
        unique_ptr<DeviceBuffer> buffer_m;      // Can be NULL e.g. local
                                                // Using unique_ptr ensures 
                                                // DeviceBuffer dtor is called 
};

inline DataMap::DataMap(MapKinds m, uintptr_t start, size_t size,
                        MapCreator c) 
                                        : host_range_m(start, size), 
                                          map_kind_m(m),
                                          map_creator_m(c),
                                          buffer_m(nullptr)
{}


inline bool DataMap::operator== (const DataMap &dm) const
{
    return host_range_m == dm.host_range_m;
}

inline bool DataMap::operator< (const DataMap &dm) const
{
    return host_range_m < dm.host_range_m;
}

inline bool DataMap::contains (const DataMap &dm) const
{
    return host_range_m.contains(dm.host_range_m);
}

inline bool DataMap::contains (const AddressRange &ar) const
{
    return host_range_m.contains(ar);
}

inline uintptr_t     DataMap::start()   const { return host_range_m.start(); }
inline size_t        DataMap::size()    const { return host_range_m.size(); }
inline MapKinds      DataMap::kind()    const { return map_kind_m; }
inline bool          DataMap::isLocal() const { return map_kind_m == mk_TO_L2;}
inline MapCreator    DataMap::creator() const { return map_creator_m; }
inline size_t        DataMap::bias()    const { ASSERT_FAIL; return 0; }
inline const size_t* DataMap::biasPtr() const { ASSERT_FAIL; return 0; }

inline bool          DataMap::isTo()    const { return map_kind_m == mk_TO ||
                                                     map_kind_m == mk_TO_FROM; 
                                              }
inline bool          DataMap::isFrom()  const { return map_kind_m == mk_FROM ||
                                                     map_kind_m == mk_TO_FROM; 
                                              }

inline DataMap::~DataMap() {}

/// Allocate storage on the device for the mapped variable
inline void DataMap::createBuffer(DeviceInterface *d)
{
    buffer_m = d->createBuffer(*this);
}

/// Variable has already been mapped in an enclosing scope, use the same 
/// underlying storage
inline void DataMap::reuseBuffer(DeviceInterface *d, const DataMap *dm)
{
    buffer_m = d->reuseBuffer(*dm);
    map_creator_m = dm->creator();
}

/// Returns a string describing the map, used only for debug messages
inline std::string DataMap::to_string() const
{
    stringstream ss;
    ss << host_range_m.to_string();
    ss << " " << MapKindNames[map_kind_m] << "," 
       << MapCreatorNames[map_creator_m];

    return ss.str();
}


/// Represents mapping a variable with map type MAP_PTR
/// The size of the variable on the device is that required to store a device
/// pointer. The bias is the adjustment that must be applied to the pointer
/// to access the variable in a target region
class DataMapPtr: public DataMap
{
    public:
        DataMapPtr(uintptr_t start, size_t bias, MapCreator c):
            DataMap(mk_MAP_PTR, start, sizeof(HostPtr), c),
            bias_m(bias)
        {}

        virtual void reuseBuffer  (DeviceInterface *d, const DataMap* dm);

        virtual size_t        bias()    const { return bias_m; }
        virtual const size_t* biasPtr() const { return &bias_m; }

    private:
        size_t bias_m;
};


/// Variable has already been mapped in an enclosing scope, use the same 
/// underlying storage
inline void DataMapPtr::reuseBuffer  (DeviceInterface *d, const DataMap* dm)
{
    DataMap::reuseBuffer(d, dm);
    bias_m = dm->bias(); 
}


} // namespace oa
