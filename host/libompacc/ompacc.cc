/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 * \file ompacc.cc
 * \brief OpenMP 4.0 Accelerator Model API 
 */

#include "ompacc.h"
#include "device.h"
#include "region.h"

#define OMP_HOST_DEVICE  (-1)

using namespace oa;

/*********************************************************************/
/* Setup data items specific to a target data region                 */
/*********************************************************************/
void GOMP_target_data (int            device,            
                       unsigned int   mapnum,   
                       void         **hostaddrs,     
                       unsigned int  *sizes,   
                       unsigned char *kinds)
{
    if (device == OMP_HOST_DEVICE)
       return;

    DataRegion *dr = new DataRegion(device);
    dr->initializeMaps(mapnum, hostaddrs, sizes, kinds);
    dr->syncDataToTarget();
    DeviceCollection::instance().device(device)->pushRegion(dr);
}

/*********************************************************************/
/* Destroy data for a target data region                             */
/*********************************************************************/
void GOMP_target_data_end (int            device,            
                           unsigned int   mapnum,   
                           void         **hostaddrs,     
                           unsigned int  *sizes,   
                           unsigned char *kinds)
{
    if (device == OMP_HOST_DEVICE)
       return;

    Region *r = DeviceCollection::instance().device(device)->peekRegion();
    r->syncDataFromTarget();
    DeviceCollection::instance().device(device)->popRegion();
}

/*********************************************************************/
/* Make a target update for given data items                         */
/*********************************************************************/
void GOMP_target_update   (int            device,            
                           unsigned int   mapnum,   
                           void         **hostaddrs,     
                           unsigned int  *sizes,   
                           unsigned char *kinds)
{
    if (device == OMP_HOST_DEVICE)
       return;

    DataRegion dr(device);
    dr.updateMaps(mapnum, hostaddrs, sizes, kinds);
}


/*********************************************************************/
/* Create a target region on the given device and execute the given  */
/* function on it                                                    */
/*********************************************************************/
void GOMP_target (int device, 
                  void (*fn) (void *),  
                  char          *fnname,  
                  unsigned int   mapnum,      
                  void         **hostaddrs,  
                  unsigned int  *sizes,    
                  unsigned char *kinds)
{
    if (device == OMP_HOST_DEVICE)
    {
       fn(hostaddrs);
       return;
    }

    TargetRegion tr(device, fn, fnname);
    tr.initializeMaps(mapnum, hostaddrs, sizes, kinds);
    tr.prepareExecutionObject();
    tr.syncDataToTarget();
    tr.triggerExecution();
    tr.syncDataFromTarget();
}

extern "C"
void __ti_ompa_record_declare_target(int device, const DeclVarMap* table)
{
    DeviceInterface *d = DeviceCollection::instance().device(device);
    DataRegion *dr = dynamic_cast<DataRegion *>(d->peekRegion());

    if (dr == NULL)
    {
        dr = new DataRegion(device);
        d->pushRegion(dr);
    }
    dr->addDeclTargetMaps(table);
}

void omp_set_default_device (int device_num)
{
}

int omp_get_default_device (void)
{
   return 0;
}

int omp_get_num_devices (void)
{
   return 1;
}

int omp_is_initial_device (void)
{
   return true;
}
