/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include <cstring>
#include "ti_omp_device.h"

void __touch(const char *p, uint32_t size) 
{
}

uint32_t __core_num(void)
{
   return 0;
}

uint32_t __clock(void)
{
   return 0;
}

uint64_t __clock64(void)
{
   return 0;
}

void __cycle_delay(uint64_t cyclesToDelay)
{
}

void __mfence(void)
{
}

void __cache_l1d_none(void)
{
}

void __cache_l1d_all(void)
{
}

void __cache_l1d_4k(void)
{
}

void __cache_l1d_8k(void)
{
}

void __cache_l1d_16k(void)
{
}

void __cache_l1d_flush(void)
{
}

void __cache_l2_none(void)
{
}

void __cache_l2_128k(void)
{
}

void __cache_l2_256k(void)
{
}

void __cache_l2_512k(void)
{
}

void __cache_l2_flush(void)
{
}

void __heap_init_ddr(void *ptr, size_t size)
{
}

void* __calloc_ddr(size_t num, size_t size)
{
   size_t i = num * size;
   void *ptr = __malloc_ddr(i);
   if (!ptr)
      return NULL;
   return memset(ptr, 0, i);
}

void* __realloc_ddr(void *ptr,  size_t size)
{
   void *p = __malloc_ddr(size);
   if (!p)
      return NULL;
   memcpy(p, ptr, size);
   __free_ddr(ptr);
   return p;
}

void* __memalign_ddr(size_t alignment, size_t size)
{
   return __malloc_ddr(size);
}

void __heap_init_msmc(void *ptr, size_t size)
{
}

void* __calloc_msmc(size_t num, size_t size)
{
   size_t i = num * size;
   void *ptr = __malloc_msmc(i);
   if (!ptr)
      return NULL;
   return memset(ptr, 0, i);
}

void* __realloc_msmc(void *ptr, size_t size)
{
   void *p = __malloc_msmc(size);
   if (!p)
      return NULL;
   memcpy(p, ptr, size);
   __free_msmc(ptr);
   return p;
}

void* __memalign_msmc(size_t alignment, size_t size)
{
   return __malloc_msmc(size);
}

void __heap_init_l2(void *ptr, size_t size)
{
}

void* __malloc_l2(size_t size)
{
   return NULL;
}
