/******************************************************************************
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Texas Instruments Incorporated nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/


#pragma once

#include "oa_cl.h"
#include "device.h"
#include "buffer.h"
#include "region.h"
#include <iostream>

namespace oa
{

/// Implements  DeviceInterface for OpenCL devices
class OclDevice: public DeviceInterface
{
    public:
        OclDevice(DeviceId id, cl_device_type type);
        virtual ~OclDevice();

        virtual void writeToDevice  (const DataMap *dm);
        virtual void readFromDevice (const DataMap *dm); 

        virtual uint64_t nameToDeviceAddress(const char *name) const;

        virtual bool buildProgram(void);

        virtual void prepareExecutionObject  (TargetRegion *er);
        virtual void preExecuteDataMovement  (TargetRegion *er);
        virtual void triggerExecution        (TargetRegion *er);
        virtual void postExecuteDataMovement (TargetRegion *er);

        virtual void waitForDataTransfer();

        virtual unique_ptr<DeviceBuffer> createBuffer (const DataMap &dm);
        virtual unique_ptr<DeviceBuffer> reuseBuffer  (const DataMap &dm);

    private:
        cl_mem_flags mapKind2memFlag(MapKinds k);

        const char * error2string(cl_int err);

        inline void errorCheck(cl_int ret)
        {
            if (ret != CL_SUCCESS)
            {
                std::cerr << "ERROR: " << error2string(ret) << std::endl;
                exit(ret);
            }
        }

        cl_context                  context_m;
        cl_command_queue            queue_m;
        cl_program                  program_m;
        cl_kernel                   kernel_m;
        cl_event                    k_event_m;
        cl_device_id                device_m;
        bool                        performed_cmemWbInvAll_m;
};

} // namespace oa
