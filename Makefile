##
##  Copyright (c) 2014-2018, Texas Instruments Incorporated
##
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions
##  are met:
##
##  *  Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##
##  *  Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in the
##     documentation and/or other materials provided with the distribution.
##
##  *  Neither the name of Texas Instruments Incorporated nor the names of
##     its contributors may be used to endorse or promote products derived
##     from this software without specific prior written permission.
##
##  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
##  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
##  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
##  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
##  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
##  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
##  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
##  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
##  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
##  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
##  ======== Makefile ========
##

.PHONY: all clean openmpacc install examples
.SILENT:

include Makefile.inc

OMPACC_DPKG_NAME 	:= ti-openmpacc
OMPACC_BUILD_DIR 	:= build/$(TARGET)
OMPACC_SRC_DIR     	:= host
OMPACC_EXAMPLES_DIR := examples

BUILD_MACHINE=$(shell uname -m)

ifeq (,$(findstring arm,$(BUILD_MACHINE)))
OMPACC_CMAKE_OPTS=-DCMAKE_TOOLCHAIN_FILE=../../${OMPACC_SRC_DIR}/CMakeARMToolChain.txt -DCROSS_BUILD=ON -DVERSION="$(OMPACC_VER)"
else
OMPACC_CMAKE_OPTS=
endif

all: openmpacc examples

examples: openmpacc
	$(MAKE) -C $(OMPACC_EXAMPLES_DIR)

openmpacc: $(OMPACC_BUILD_DIR)/Makefile
	$(MAKE) -C $(OMPACC_BUILD_DIR)

$(OMPACC_BUILD_DIR)/Makefile: $(filter-out $(wildcard $(OMPACC_BUILD_DIR)), $(OMPACC_BUILD_DIR))
	cd $(OMPACC_BUILD_DIR); cmake $(OMPACC_CMAKE_OPTS) ../../${OMPACC_SRC_DIR}

$(OMPACC_BUILD_DIR):
	$(eval OMPACC_VER := $(shell cat $(OMPACC_SRC_DIR)/version.txt))
	mkdir -p $(OMPACC_BUILD_DIR)

install: openmpacc
	$(MAKE) -C ${OMPACC_BUILD_DIR} install

clean:
	$(MAKE) -C $(OMPACC_SRC_DIR)/clacc clean
	rm -rf build/*

fresh: clean install

ifneq ($(ARM_GCC_DIR),)
export PATH:=$(ARM_GCC_DIR)/bin:$(PATH)
endif

OMPACC_VER := $(shell cat $(OMPACC_SRC_DIR)/version.txt)

export LINUX_DEVKIT_ROOT
export DESTDIR?=$(CURDIR)/install/$(TARGET)

realclean: clean
	$(MAKE) -C $(OMPACC_SRC_DIR)/boost clean
