OpenMP Accelerator Model Runtime

Building the runtime
1. Update variables in host/Makefile.inc based on the location of
   Processor SDK (SDOMC_SHARED, CORESDK_VERSION)

2. To build for a given SoC, run one of:
   * make BUILD_AM57=1 BUILD_BOOST=1 install
   * make BUILD_K2H=1 BUILD_BOOST=1 install
   Note: BUILD_BOOST=1 will download and build the required Boost version.
